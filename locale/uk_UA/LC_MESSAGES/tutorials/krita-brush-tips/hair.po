# Translation of docs_krita_org_tutorials___krita-brush-tips___hair.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___hair\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_01.png\n"
"   :alt: some examples of hair brush"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-hair_01.png\n"
"   :alt: Приклади волосяного пензля"

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_02.png\n"
"   :alt: curve setting in brush editor"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-hair_02.png\n"
"   :alt: Параметри кривої у редакторі пензлів"

#: ../../tutorials/krita-brush-tips/hair.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-hair_03.png\n"
"   :alt: brush-tip dialog"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-hair_03.png\n"
"   :alt: Діалогове вікно кінчика пензля"

#: ../../tutorials/krita-brush-tips/hair.rst:1
msgid "A tutorial about painting hair in Krita"
msgstr "Підручник із малювання волосся у Krita"

#: ../../tutorials/krita-brush-tips/hair.rst:13
msgid "Brush-tips:Hair"
msgstr "Кінчики пензлів: Волосся"

#: ../../tutorials/krita-brush-tips/hair.rst:18
msgid ""
"Usually, most digital styles tend to focus on simple brushes, like the round "
"brushes, and their usage in hair is no different. So, the typical example "
"would be the one on the left, where we use *fill_round* to draw a silhouette "
"and build up to lighter values."
msgstr ""
"Зазвичай, у більшості цифрових стилів акцент роблять на простих пензлях, "
"зокрема круглих пензлів, і їхнє використання у волоссі не відрізняється. "
"Отже, типовим прикладом буде те, що намальовано ліворуч, де ми скористалися "
"*fill_round* для малювання силуету і домалювали зображення до світліших "
"кольорів."

#: ../../tutorials/krita-brush-tips/hair.rst:20
msgid ""
"The reason I use *fill_round* here is because the pressure curve on the size "
"is s-shaped. My tablet has a spring-loaded nib which also causes and s-"
"shaped curve hard-ware wise. This means that it becomes really easy to draw "
"thin lines and big lines. Having a trained inking hand helps a lot with this "
"as well, and it’s something you build up over time."
msgstr ""
"Причиною для використання у цьому прикладі *fill_round* було те, що крива "
"тиску для розміру тут є S-подібною. На стилі планшета автора є підпружинена "
"кнопка, яка реалізує S-подібну криву на апаратному рівні. Це означає, що за "
"його допомогою дуже просто малювати тонкі лінії і товсті лінії. Вправність у "
"малюванні графіки також дуже важлива — з часом ви обов'язково її набудете."

#: ../../tutorials/krita-brush-tips/hair.rst:25
msgid ""
"We then gloss the shadow parties with the *basic_tip_default*. So you can "
"get really far with basic brushes and basic painting skills and indeed I am "
"almost convinced tysontan, who draws our mascot, doesn’t use anything but "
"the *basic_tip_default* sometimes."
msgstr ""
"Далі, ми навели блиск на затінені ділянки за допомогою *basic_tip_default*. "
"Отже, навіть за допомогою базових пензлів і базових навичок малювання можна "
"отримати доволі пристойні результати. Автор майже певен, що tysontan, "
"художник, який намалював наш маскот, іноді використовує у творі лише "
"*basic_tip_default*."

#: ../../tutorials/krita-brush-tips/hair.rst:30
msgid ""
"However, if you want an easy hair brush, just take the *fill_round*, go to "
"the brush-tip, pick predefined and select *A2-sparkle-1* as the brush tip. "
"You can fiddle with the spacing below the selection of predefined brushtip "
"to space the brush, but I believe the default should be fine enough to get "
"result."
msgstr ""
"Втім, якщо вам потрібен простий у користуванні пензель для малювання "
"волосся, просто виберіть *fill_round*, перейдіть до вибору кінчика пензля, "
"виберіть попередньо визначні і скористайтеся пунктом *A2-sparkle-1*. Можете "
"трохи погратися із інтервалом під панеллю вибору попередньо визначеного "
"кінчика пензля, але нам здається, що типового значення достатньо для "
"отримання результатів."
