# Translation of docs_krita_org_reference_manual___filters___blur.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___blur\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ".. image:: images/filters/Lens-blur-filter.png"

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr "Огляд фільтрів розмивання."

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "Розмивання"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "Гаусове розмивання"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left."
msgstr ""
"Фільтрами розмивання часто користуються для згладжування жорстких країв або "
"деталей на зображеннях. Результат буде розмитим. Нижче наведено приклад "
"розмитого зображення. Зображення Кікі праворуч є результатом застосування "
"фільтра розмивання до зображення, яке наведено ліворуч."

#: ../../reference_manual/filters/blur.rst:21
msgid ".. image:: images/filters/Blur.png"
msgstr ".. image:: images/filters/Blur.png"

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr "Передбачено багато різних фільтрів для розмивання:"

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr ""
"Тут ви можете вказати горизонтальний і вертикальний радіус для ділянки "
"розмивання.."

#: ../../reference_manual/filters/blur.rst:30
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ".. image:: images/filters/Gaussian-blur.png"

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "Розмивання рухом"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""
"Виконати не лише розмивання, але і легке розмазування зображення у напрямку, "
"який вказано кутом, таким чином надавши елементу зображення подібності руху. "
"Цей фільтр часто використовують для створення ефектів об'єктів, які швидко "
"рухаються."

#: ../../reference_manual/filters/blur.rst:37
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ".. image:: images/filters/Motion-blur.png"

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur."
msgstr "За допомогою цього фільтра можна створити звичайне розмивання."

#: ../../reference_manual/filters/blur.rst:44
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ".. image:: images/filters/Blur-filter.png"

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "Розмивання об’єктивом"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr "Алгоритм розмивання розфокусованим об'єктивом."
