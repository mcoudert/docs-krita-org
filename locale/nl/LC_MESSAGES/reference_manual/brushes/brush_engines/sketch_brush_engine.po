# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 16:28+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Paint Connection Line"
msgstr "Verbindingslijn tekenen"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:1
msgid "The Sketch Brush Engine manual page."
msgstr "De handleidingpagina Schetspenseel-engine."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:17
msgid "Sketch Brush Engine"
msgstr "Schetspenseel-engine"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:12
msgid "Harmony Brush Engine"
msgstr "Harmony-penseel-engine"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:21
msgid ".. image:: images/icons/sketchbrush.svg"
msgstr ".. image:: images/icons/sketchbrush.svg"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:22
msgid ""
"A line based brush engine, based on the Harmony brushes. Very messy and fun."
msgstr ""
"Een penseel-engine gebaseerd op een lijn, gebaseerd op de Harmony-penselen. "
"Zeer rommelig en plezierig."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:25
msgid "Parameters"
msgstr "Parameters"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:28
msgid "Has the following parameters:"
msgstr "Heeft de volgende parameters:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:30
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:31
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:32
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:33
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:34
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:35
msgid ":ref:`option_line_width`"
msgstr ":ref:`option_line_width`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:36
msgid ":ref:`option_offset_scale`"
msgstr ":ref:`option_offset_scale`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:37
msgid ":ref:`option_sketch_density`"
msgstr ":ref:`option_sketch_density`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:38
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:39
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:44
msgid "Line Width"
msgstr "Lijndikte"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:46
msgid "The width of the rendered lines."
msgstr "De breedte van de gemaakte lijnen."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:49
msgid ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_sketch_linewidth.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:53
msgid "Offset Scale"
msgstr "Offsetschaal"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:55
msgid ""
"When curve lines are formed, this value roughly determines the distance from "
"the curve lines to the connection lines:"
msgstr ""
"Wanneer kromme lijnen worden gevormd bepaalt deze waarde ruwweg de afstand "
"van de gekromde lijnen naar de verbindingslijnen:"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:57
msgid ""
"This is a bit misleading, because a value of 0% and a value of 100% give "
"similar outputs, as do a value of say 30% and 70%. You could think that the "
"actual value range is between 50% and 200%."
msgstr ""
"Dit is een beetje misleidend, omdat een waarde van 0% en een waarde van 100% "
"gelijke uitvoer geven, evenals een waarde van zeg 30% en 70%. U zou kunnen "
"denken dat de werkelijke waardereeks ligt tussen 50% en 200%."

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:58
msgid ""
"0% and 100% correspond to the curve lines touching the connection lines "
"exactly."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:59
msgid ""
"Above 100%, the curve lines will go further than the connection lines, "
"forming a fuzzy effect."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_offset.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:64
msgid ".. image:: images/brushes/Krita-sketch_offset_scale2.png"
msgstr ".. image:: images/brushes/Krita-sketch_offset_scale2.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:68
msgid "Density"
msgstr "Dichtheid"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:70
msgid ""
"The density of the lines. This one is highly affected by the Brush-tip, as "
"determined by the Distance Density toggle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:73
msgid ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"
msgstr ".. image:: images/brushes/Krita_2.9_brushengine_sketch_density.png"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:74
msgid "Use Distance Density"
msgstr "Afstandsdichtheid gebruiken"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:75
msgid ""
"The further the line covered is from the center of the area of effect, the "
"less the density of the resulting curve lines."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid "Magnetify"
msgstr "Vergroten"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:77
msgid ""
"Magnetify is *on* by default. It's what causes curve lines to form between "
"two close line sections, as though the curve lines are attracted to them "
"like magnets. With Magnetify *off*, the curve line just forms on either side "
"of the current active portion of your connection line. In other words, your "
"line becomes fuzzier when another portion of the line is nearby, but the "
"lines don't connect to said previous portion."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:79
msgid "Random RGB"
msgstr "Willekeurige RGB"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:80
msgid "Causes some slight RGB variations."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:81
msgid "Random Opacity"
msgstr "Willekeurige dekking"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:82
msgid ""
"The curve lines get random opacity. This one is barely visible, so for the "
"example I used line width 12 and 100% opacity."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:83
msgid "Distance Opacity"
msgstr "Afstandsdekking"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:84
msgid ""
"The distance based opacity. When you move your pen fast when painting, the "
"opacity will be calculated based on the distance from the center of the "
"effect area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:85
msgid "Simple Mode"
msgstr "Eenvoudige modus"

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:86
msgid ""
"This mode exists for performance reasons, and doesn't affect the output in a "
"visible way. Check this for large brushes or thick lines for faster "
"rendering."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/sketch_brush_engine.rst:88
msgid ""
"What appears to be the connection line is usually made up of an actual "
"connection line and many smaller curve lines. The many small curve lines "
"make up the majority of the line. For this reason, the only time this option "
"will make a visible difference is if you're drawing with 0% or near 0% "
"density, and with a thick line width. The rest of the time, this option "
"won't make a visible difference."
msgstr ""
