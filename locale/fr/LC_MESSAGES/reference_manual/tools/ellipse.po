# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-03-12 00:03+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:28
msgid ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_circle.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_from_center.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_reposition.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:1
msgid "Krita's ellipse tool reference."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:11
#, fuzzy
#| msgid "Ellipse Tool"
msgid "Ellipse"
msgstr "Ellipse"

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Circle"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:16
msgid "Ellipse Tool"
msgstr "Ellipse"

#: ../../reference_manual/tools/ellipse.rst:18
msgid "|toolellipse|"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:20
msgid ""
"Use this tool to paint an ellipse. The currently selected brush is used for "
"drawing the ellipse outline. Click and hold the left mouse button to "
"indicate one corner of the ‘bounding rectangle’ of the ellipse, then move "
"your mouse to the opposite corner. :program:`Krita` will show a preview of "
"the ellipse using a thin line. Release the button to draw the ellipse."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:22
msgid ""
"While dragging the ellipse, you can use different modifiers to control the "
"size and position of your ellipse:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:24
msgid ""
"In order to make a circle instead of an ellipse, hold the :kbd:`Shift` key "
"while dragging. After releasing the :kbd:`Shift` key any movement of the "
"mouse will give you an ellipse again:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:29
msgid ""
"In order to keep the center of the ellipse fixed and only growing and "
"shrinking the ellipse around it, hold the :kbd:`Ctrl` key while dragging:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:34
msgid "In order to move the ellipse around, hold the :kbd:`Alt` key:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:39
msgid ""
"You can change between the corner/corner and center/corner dragging methods "
"as often as you want by holding down or releasing the :kbd:`Ctrl` key, "
"provided you keep the left mouse button pressed. With the :kbd:`Ctrl` key "
"pressed, mouse movements will affect all four corners of the bounding "
"rectangle (relative to the center), without the :kbd:`Ctrl` key, the corner "
"opposite to the one you are moving remains still. With the :kbd:`Alt` key "
"pressed, all four corners will be affected, but the size stays the same."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:42
msgid "Tool Options"
msgstr "Préférences d'outils"
