msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___dyna_brush_engine."
"pot\n"

#: ../../<generated>:1
msgid "Paint Connection"
msgstr "绘制连接线。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:1
msgid "The Dyna Brush Engine manual page."
msgstr "介绍 Krita 的力学笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:15
msgid "Dyna Brush Engine"
msgstr "力学笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:18
msgid ".. image:: images/icons/dynabrush.svg"
msgstr ".. image:: images/icons/dynabrush.svg"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:19
msgid ""
"Dyna brush uses dynamic setting like mass and drag to draw strokes. The "
"results are fun and random spinning strokes. To experiment more with this "
"brush you can play with values in 'dynamic settings' section of the brush "
"editor under Dyna Brush."
msgstr ""
"力学笔刷使用和力学有关的选项，包括重量和和拽引等，能够模拟笔尖的重量，减少或"
"者增加笔尖的甩动。配合传感器使用，可以对画出的效果进行微调。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:23
msgid ""
"This brush engine has been removed in 4.0. This engine mostly had smoothing "
"results that the dyna brush tool has in the toolbox. The stabilizer settings "
"can also give you further smoothing options from the tool options."
msgstr ""
"此笔刷引擎已经在 4.0 版 中移除。现在工具箱里面的 :ref:`dyna_tool` 可以实现此"
"引擎的全部功能， :ref:`freehand_brush_tool` 也可以通过防抖选项画出类似的平滑"
"笔画。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:26
msgid "Options"
msgstr "可用选项"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:28
msgid ":ref:`option_size_dyna`"
msgstr ":ref:`option_size_dyna`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:31
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:36
msgid "Brush Size (Dyna)"
msgstr "笔刷大小 (力学)"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:39
msgid "Dynamics Settings"
msgstr "动态选项"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:41
msgid "Initial Width"
msgstr "起始宽度"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:42
msgid "Initial size of the dab."
msgstr "笔尖印迹的起始宽度。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:43
msgid "Mass"
msgstr "重量"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:44
msgid "How much energy there is in the satellite like movement."
msgstr "笔尖的重量，用于计算笔尖的惯性，得到甩动的效果。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:45
msgid "Drag"
msgstr "拽引"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:46
msgid "How close the dabs follow the position of the brush-cursor."
msgstr "笔尖印迹返回笔刷光标下方的速度。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "Width Range"
msgstr "宽度范围"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "How much the dab expands with speed."
msgstr "笔尖印迹随速度加宽的程度。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:51
msgid "Shape"
msgstr "形状"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:53
msgid "Diameter"
msgstr "直径"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:54
msgid "Size of the shape."
msgstr "笔尖形状的大小。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:55
msgid "Angle"
msgstr "角度"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:56
msgid "Angle of the shape. Requires Fixed Angle active to work."
msgstr "笔尖形状的朝向角度，必须启用笔尖的固定角度选项。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:57
msgid "Circle"
msgstr "圆形笔尖"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:58
msgid "Make a circular dab appear."
msgstr "显示一个圆形的笔尖。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:59
msgid "Two"
msgstr "双重"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:60
msgid "Draws an extra circle between other circles."
msgstr "在与另一个圆形笔尖之间再绘制一个圆形笔尖。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:61
msgid "Line"
msgstr "线条"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:62
msgid ""
"Connecting lines are drawn next to each other. The number boxes on the right "
"allows you to set the spacing between the lines and how many are drawn."
msgstr "绘制相邻的线条，右边的数字输入框控制线条的间距和绘制数量。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:63
msgid "Polygon"
msgstr "多边形"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:64
msgid "Draws a black polygon as dab."
msgstr "以黑色的多边形作为笔尖。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:65
msgid "Wire"
msgstr "线网"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:66
msgid "Draws the wireframe of the polygon."
msgstr "绘制多边形的线网。"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:68
msgid "Draws the connection line."
msgstr "绘制出连接线。"
