# Translation of docs_krita_org_tutorials___making_an_azalea_with_the_transformation_masks.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-24 17:05+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../<rst_epilog>:46
msgid ""
".. image:: images/icons/transform_tool.svg\n"
"   :alt: tooltransform"
msgstr ""
".. image:: images/icons/transform_tool.svg\n"
"   :alt: eina de transformació"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Krita-screencast-azaleas.png\n"
"   :alt: making azalea with transform masks"
msgstr ""
".. image:: images/making-azalea/Krita-screencast-azaleas.png\n"
"   :alt: Creant una azalea amb màscares de transformació."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_01_trunk-.png\n"
"   :alt: starting with the trunk and reference image"
msgstr ""
".. image:: images/making-azalea/Azelea_01_trunk-.png\n"
"   :alt: Començant amb el tronc i la imatge de referència."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_02_drawing-flowers.png\n"
"   :alt: making the outline of the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_02_drawing-flowers.png\n"
"   :alt: Fent el contorn de les flors."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_03_filling-flowers.png\n"
"   :alt: coloring the details and filling the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_03_filling-flowers.png\n"
"   :alt: Donant color als detalls i emplenant les flors."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_04_finished-setup.png\n"
"   :alt: finished setup for making azalea"
msgstr ""
".. image:: images/making-azalea/Azelea_04_finished-setup.png\n"
"   :alt: Configuració finalitzada per a crear l'azalea."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_05_clonelayer.png\n"
"   :alt: create clone layers of the flowers"
msgstr ""
".. image:: images/making-azalea/Azelea_05_clonelayer.png\n"
"   :alt: Creant les capes clonades de les flors."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_06_transformmask.png\n"
"   :alt: adding transform masks to the cloned layers"
msgstr ""
".. image:: images/making-azalea/Azelea_06_transformmask.png\n"
"   :alt: Afegint les màscares de transformació a les capes clonades."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_07_clusters.png\n"
"   :alt: adding more clusters"
msgstr ""
".. image:: images/making-azalea/Azelea_07_clusters.png\n"
"   :alt: Afegint més grups."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_08_leaves.png\n"
"   :alt: making leaves"
msgstr ""
".. image:: images/making-azalea/Azelea_08_leaves.png\n"
"   :alt: Creant les fulles."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_09_paintingoriginals.png\n"
"   :alt: painting originals"
msgstr ""
".. image:: images/making-azalea/Azelea_09_paintingoriginals.png\n"
"   :alt: Pintant els originals."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_10_alphainheritance_1.png\n"
"   :alt: using the alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_10_alphainheritance_1.png\n"
"   :alt: Utilitzant l'herència alfa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_11_alphainheritance_2.png\n"
"   :alt: clipping the cluster with alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_11_alphainheritance_2.png\n"
"   :alt: Retallant els grups amb l'herència alfa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_12_alphainheritance_3.png\n"
"   :alt: activate alpha inheritance"
msgstr ""
".. image:: images/making-azalea/Azelea_12_alphainheritance_3.png\n"
"   :alt: Activant l'herència alfa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_13_alphainheritance_4.png\n"
"   :alt: multiplying the clipped shape"
msgstr ""
".. image:: images/making-azalea/Azelea_13_alphainheritance_4.png\n"
"   :alt: Multiplicant la forma retallada."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_14_alphainheritance_5.png\n"
"   :alt: remove extra areas with the eraser"
msgstr ""
".. image:: images/making-azalea/Azelea_14_alphainheritance_5.png\n"
"   :alt: Eliminant les àrees addicionals amb l'esborrador."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:None
msgid ""
".. image:: images/making-azalea/Azelea_15_alphainheritance_6.png\n"
"   :alt: add shadows and highlights with alpha inheritance technique"
msgstr ""
".. image:: images/making-azalea/Azelea_15_alphainheritance_6.png\n"
"   :alt: Afegint ombres i llums amb la tècnica de l'herència alfa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:1
msgid "Tutorial for making azalea with the help of transform masks"
msgstr ""
"Guia d'aprenentatge per a crear una azalea amb l'ajuda de les màscares de "
"transformació"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:13
msgid "Making An Azalea With The Transformation Masks"
msgstr "Crear una azalea amb les màscares de transformació"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:18
msgid "This page was ported from the original post on the main page"
msgstr ""
"Aquesta pàgina s'ha adaptat des d'una publicació original a la pàgina "
"principal del Krita a KDE UserBase."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:20
msgid ""
"Okay, so I’ve wanted to do a tutorial for transform masks for a while now, "
"and this is sorta ending up to be a flower-drawing tutorial. Do note that "
"this tutorial requires you to use **Krita 2.9.4 at MINIMUM**. It has a "
"certain speed-up that allows you to work with transform masks reliably!"
msgstr ""
"D'acord, fa temps que volia fer una guia d'aprenentatge per a les màscares "
"de transformació, i aquesta acabarà sent una guia d'aprenentatge per al "
"dibuix de flors. Recordeu que es requereix que utilitzeu **com a MÍNIM el "
"Krita 2.9.4**. Conté una certa acceleració que permet treballar amb les "
"màscares de transformació de manera fiable!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:22
msgid ""
"I like drawing flowers because they are a bit of an unappreciated subject, "
"yet allow for a lot of practice in terms of rendering. Also, you can explore "
"cool tricks in Krita with them."
msgstr ""
"M'agrada dibuixar flors perquè són un tema poc apreciat, però permeten molta "
"pràctica en termes de representació. A més, amb elles podreu explorar trucs "
"genials en el Krita."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:24
msgid ""
"Today’s flower is the Azalea flower. These flowers are usually pink to red "
"and appear in clusters, the clusters allow me to exercise with transform "
"masks!"
msgstr ""
"La flor d'avui és una azalea. Aquestes flors generalment són de color rosa a "
"vermell i apareixen en grups. Els grups em permeten exercitar amb les "
"màscares de transformació!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:26
msgid ""
"I got an image from Wikipedia for reference, mostly because it’s public "
"domain, and as an artist I find it important to respect other artists. You "
"can copy it and, if you already have a canvas, :menuselection:`Edit --> "
"Paste into New Image` or :menuselection:`New --> Create from Clipboard`."
msgstr ""
"He obtingut una imatge de la Wikipedia com a referència, principalment "
"perquè és de domini públic, i com a artista considero important respectar "
"els altres artistes. Podreu copiar-la i, si ja teniu un llenç, feu :"
"menuselection:`Edita --> Enganxa en una imatge nova` o :menuselection:`Nova "
"--> Crea des del porta-retalls`."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:28
msgid ""
"Then, if you didn’t have a new canvas make one. I made an A5 300dpi canvas. "
"This is not very big, but we’re only practicing. I also have the background "
"color set to a yellow-grayish color (#CAC5B3), partly because it reminds me "
"of paper, and partly because bright screen white can strain the eyes and "
"make it difficult to focus on values and colors while painting. Also, due to "
"the lack of strain on the eyes, you’ll find yourself soothed a bit. Other "
"artists use #c0c0c0, or even more different values."
msgstr ""
"Llavors, si no teniu un llenç nou, creeu-ne un. He creat un llenç A5 amb 300 "
"ppp. Aquest no és gaire gran, però només estem practicant. També tinc el "
"color de fons establert a un color groc grisenc (#CAC5B3), en part perquè em "
"recorda al paper, i en part perquè el blanc brillant de la pantalla pot "
"danyar els ulls i dificultar l'enfocament en els valors i colors mentre es "
"pinta. A més, a causa de la falta de tensió en els ulls, us sentireu més "
"alleujats. Altres artistes utilitzen #C0C0C0, o fins i tot valors més "
"diferents."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:30
msgid ""
"So, if you go to :menuselection:`Window --> Tile`, you will find that now "
"your reference image and your working canvas are side by side. The reason I "
"am using this instead of the docker is because I am lazy and don’t feel like "
"saving the wikipedia image. We’re not going to touch the image much."
msgstr ""
"De manera que si aneu a :menuselection:`Finestra --> Mosaic`, trobareu que "
"ara la imatge de referència i la del llenç de treball estan una al costat de "
"l'altra. La raó per la qual estic emprant això en lloc de l'acoblador és "
"perquè sóc mandrós i no tinc ganes de desar la imatge de la Wikipedia. No la "
"tocarem massa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:33
msgid "Let’s get to drawing!"
msgstr "Anem a dibuixar!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:38
msgid ""
"First we make a bunch of branches. I picked a slightly darker color here "
"than usual, because I know that I’ll be painting over these branches with "
"the lighter colors later on. Look at the reference how branches are formed."
msgstr ""
"Primer crearem un munt de branques. Aquí he triat un color lleugerament més "
"fosc del que és habitual, perquè sé que més endavant pintaré sobre aquestes "
"branques amb colors més clars. Mireu a la referència per a veure com estan "
"formades les branques."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:43
msgid ""
"Then we make an approximation of a single flower on a layer. We make a few "
"of these, all on separate layers. We also do not color pick the red, but we "
"guess at it. This is good practice, so we can learn to analyze a color as "
"well as how to use our color selector. If we’d only pick colors, it would be "
"difficult to understand the relationship between them, so it’s best to "
"attempt matching them by eye."
msgstr ""
"A continuació farem una aproximació d'una sola flor en una capa. En crearem "
"unes quantes, totes en capes separades. Tampoc hem triat el color vermell, "
"però l'endevinarem. Aquesta és una bona pràctica, de manera que podrem "
"aprendre a analitzar un color i a utilitzar el nostre selector de color. Si "
"només agaféssim els colors de la imatge de referència, seria difícil "
"entendre la relació entre ells, de manera que és millor intentar combinar-"
"los a ull."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:48
msgid ""
"I chose to make the flower shape opaque quickly by using the *behind* "
"blending mode. This’ll mean Krita is painting the new pixels behind the old "
"ones. Very useful for quickly filling up shapes, just don’t forget to go "
"back to *normal* once you’re done."
msgstr ""
"He escollit crear la forma de la flor opaca de manera ràpida utilitzant el "
"mode de barreja *Darrere*. Això vol dir que el Krita pintarà els píxels nous "
"al darrere dels vells. Molt útil per emplenar ràpidament les formes, "
"simplement no oblideu tornar a *Normal* una vegada hàgiu acabat."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:53
msgid ""
"Now, we’ll put the flowers in the upper left corner, and group them. You can "
"group by making a group layer, and selecting the flower layers in your "
"docker with the :kbd:`Ctrl +` |mouseleft| shortcut and dragging them into "
"the group. The reason why we’re putting them in the upper left corner is "
"because we’ll be selecting them a lot, and Krita allows you to select layers "
"with the :kbd:`R +` |mouseleft| shortcut on the canvas quickly. Just hold "
"the :kbd:`R` key and |mouseleft| the pixels belonging to the layer you want, "
"and Krita will select the layer in the Layer docker."
msgstr ""
"Ara, col·locarem les flors a la cantonada superior esquerra i les agruparem. "
"Les podreu agrupar creant una capa de grup i seleccionant les capes amb "
"flors a l'acoblador amb la drecera :kbd:`Ctrl + fent` |mouseleft| i "
"arrossegar-la cap al grup. La raó per la qual les col·loquem a la cantonada "
"superior esquerra és perquè les seleccionarem molt, i el Krita permet "
"seleccionar ràpidament les capes amb la drecera :kbd:`R + fent` |mouseleft| "
"sobre el llenç. Simplement premeu la tecla :kbd:`R` i feu |mouseleft| sobre "
"els píxels que pertanyen a la capa que voleu, i el Krita seleccionarà la "
"capa a l'acoblador Capes."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:56
msgid "Clone Layers"
msgstr "Capes clonades"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:58
msgid ""
"Now, we will make clusters. What we’ll be doing is that we select a given "
"flower and then make a new clone layer. A clone layer is a layer that is "
"literally a clone of the original. They can’t be edited themselves, but edit "
"the original and the clone layer will follow suit. Clone Layers, and File "
"layers, are our greatest friends when it comes to transform masks, and "
"you’ll see why in a moment."
msgstr ""
"Ara, crearem grups. El que farem és seleccionar una flor determinada i "
"després crear una capa clonada nova. Una capa clonada és una capa que és "
"literalment un clon de l'original. No es poden editar, però si editeu "
"l'original, la capa clonada seguirà aquests canvis. Les Capes clonades i les "
"Capes de fitxer són els nostres millors amics quan es tracta de les màscares "
"de transformació, i veureu perquè en un moment."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:63
msgid ""
"You’ll quickly notice that our flowers are not good enough for a cluster: we "
"need far more angles on the profile for example. If only there was a way to "
"transform them… but we can’t do that with clone layers. Or can we?"
msgstr ""
"Notareu ràpidament que les nostres flors no són prou bones per a un grup: "
"per exemple, necessitarem molts més angles en el perfil. Si només hi hagués "
"una manera de transformar-les... però no podem fer-ho amb les capes "
"clonades. O sí podem?"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:66
msgid "Enter Transform Masks!"
msgstr "Comencem a utilitzar les màscares de transformació!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:68
msgid ""
"Transform Masks are a really powerful feature introduced in 2.9. They are in "
"fact so powerful, that when you first use them, you can’t even begin to "
"grasp where to use them."
msgstr ""
"Les Màscares de transformació són una característica realment poderosa "
"introduïda en la versió 2.9. De fet, són tan poderoses que quan les "
"utilitzeu per primera vegada, ni tan sols començareu a comprendre on "
"utilitzar-les."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:70
msgid ""
"Transform masks allow us to do a transform operation onto a layer, any given "
"layer, and have it be completely dynamic! This includes our clone layer "
"flowers!"
msgstr ""
"Les màscares de transformació ens permeten realitzar una operació de "
"transformació dins d'una capa, sobre qualsevol capa. Fent que sigui "
"completament dinàmica! Això inclou les nostres flors a la capa clonada!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:72
msgid "How to use them:"
msgstr "Com utilitzar-les:"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:74
msgid ""
"|mouseright| the layer you want to do the transform on, and add a "
"**Transform mask.**"
msgstr ""
"Feu |mouseright| sobre la capa en la qual voleu realitzar la transformació i "
"afegiu una **Màscara de transformació**."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:76
msgid ""
"A transform mask should now have been added. You can recognize them by the "
"little ‘scissor’ icon."
msgstr ""
"Ara hi hauria d'haver afegida una màscara de transformació. La reconeixereu "
"per la petita icona de «tisora»."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:81
msgid ""
"Now, with the transform mask selected, select the |tooltransform|, and "
"rotate our clone layer. Apply the transform. You know you’re successful when "
"you can hide the transform mask, and the layer goes back to its original "
"state!"
msgstr ""
"Ara, amb la màscara de transformació seleccionada, seleccioneu |"
"tooltransform| i feu girar la capa clonada. Apliqueu la transformació. "
"Sabreu que heu tingut èxit quan pugueu ocultar la màscara de transformació i "
"la capa torni al seu estat original!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:83
msgid ""
"You can even go and edit your transform! Just activate the |tooltransform| "
"again while on a transform mask, and you will see the original transform so "
"you can edit it. If you go to a different transform operation however, you "
"will reset the transform completely, so watch out."
msgstr ""
"Fins i tot podreu editar la vostra transformació! Simplement activeu de nou |"
"tooltransform| mentre esteu sobre una màscara de transformació, i veureu la "
"transformació original de manera que podreu editar-la. No obstant això, si "
"feu una operació de transformació diferent, restablireu del tot la "
"transformació, així que aneu amb compte."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:88
msgid ""
"We’ll be only using affine transformations in this tutorial (which are the "
"regular and perspective transform), but this can also be done with warp, "
"cage and liquify, which’ll have a bit of a delay (3 seconds to be precise). "
"This is to prevent your computer from being over-occupied with these more "
"complex transforms, so you can keep on painting."
msgstr ""
"Només utilitzarem transformacions afins a aquesta guia d'aprenentatge (les "
"quals són les transformacions Normal i en Perspectiva), però això també es "
"pot fer amb Deformació, Gàbia i Liqüescent, les quals tindran una mica de "
"retard (3 segons per a ser precisos). És per evitar que el vostre ordinador "
"se sobre ocupi amb aquestes transformacions més complexes, de manera que "
"pugueu seguir pintant."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:90
msgid "We continue on making our clusters till we have a nice arrangement."
msgstr ""
"Continuarem creant els nostres grups fins que tinguem un bon arranjament."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:95
msgid "Now do the same thing for the leaves."
msgstr "Ara feu el mateix amb les fulles."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:100
msgid ""
"Now, if you select the original paint layers and draw on them, you can see "
"that all clone masks are immediately updated!"
msgstr ""
"Ara, si seleccioneu les capes de pintura originals i dibuixeu sobre seu, "
"veureu que totes les màscares clonades s'actualitzen immediatament!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:102
msgid ""
"Above you can see there’s been a new view added so we can focus on painting "
"the flower and at the same time see how it’ll look. You can make a new view "
"by going :menuselection:`Window --> New View` and selecting the name of your "
"current canvas (save first!). Views can be rotated and mirrored differently."
msgstr ""
"A dalt veureu que s'ha afegit una vista nova perquè puguem centrar-nos en "
"pintar la flor i alhora veure com es veurà. Podeu crear una vista nova anant "
"a :menuselection:`Finestra --> Vista nova` i seleccionant el nom del vostre "
"llenç actual (primer feu un desament!). Les vistes es poden girar i "
"emmirallar."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:104
msgid ""
"Now continue painting the original flowers and leaves, and we’ll move over "
"to adding extra shadow to make it seem more lifelike!"
msgstr ""
"Ara continueu pintant les flors i fulles originals, i passarem a afegir una "
"ombra addicional perquè sembli més real!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:109
msgid ""
"We’re now going to use *Alpha Inheritance*. Alpha inheritance is an ill-"
"understood concept, because a lot of programs use *clipping masks* instead, "
"which clip the layer’s alpha using only the alpha of the first next layer."
msgstr ""
"Ara utilitzarem l'*Herència alfa*. És un concepte mal entès, perquè molts "
"programes utilitzen *màscares de retallat*, les quals escurçaran l'alfa de "
"la capa només emprant l'alfa de la primera capa següent."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:111
msgid ""
"Alpha inheritance, however, uses all layers in a stack, so all the layers in "
"the group that haven’t got alpha inheritance active themselves, or all the "
"layers in the stack when the layer isn’t in a group. Because most people "
"have an opaque layer at the bottom of their layer stack, alpha inheritance "
"doesn’t seem to do much."
msgstr ""
"No obstant això, l'herència alfa utilitza totes les capes d'una pila, de "
"manera que no totes les capes del grup tindran activa l'herència alfa o "
"totes les capes de la pila quan la capa no estigui en un grup. A causa que "
"la majoria de les persones té una capa opaca a la part inferior de la seva "
"pila de capes, l'herència alfa no semblarà fer gaire."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:113
msgid ""
"But for us, alpha inheritance is useful, because we can use all clone-layers "
"in a cluster (if you grouped them), transformed or not, for clipping. Just "
"draw a light blue square over all the flowers in a given cluster."
msgstr ""
"Però per a nosaltres, l'herència alfa és útil, perquè podem emprar totes les "
"capes clonades en un grup (si les agrupem), transformades o no, per a "
"retallar. Simplement, dibuixeu un quadrat blau clar sobre totes les flors en "
"algun grup."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:118
msgid ""
"Then press the last icon in the layer stack, the alpha-inherit button, to "
"activate alpha-inheritance."
msgstr ""
"Després premeu l'última icona a la pila de capes, el botó d'herència alfa, "
"per activar l'herència alfa."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:123
msgid ""
"Set the layer to *multiply* then, so it’ll look like everything’s darker "
"blue."
msgstr ""
"Després establiu la capa a *Multiplica*, de manera que es veurà tot com amb "
"un blau més fosc."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:128
msgid ""
"Then, with multiply and alpha inheritance on, use an eraser to remove the "
"areas where there should be no shadow."
msgstr ""
"Després, amb Multiplica i l'Herència alfa activades, utilitzeu un esborrador "
"per eliminar les àrees on no hi hauria d'haver una ombra."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:133
msgid ""
"For the highlights use exactly the same method, AND exactly the same color, "
"but instead set the layer to Divide (you can find this amongst the "
"Arithmetic blending modes). Using Divide has exactly the opposite effect as "
"using multiply with the same color. The benefit of this is that you can "
"easily set up a complementary harmony in your shadows and highlights using "
"these two."
msgstr ""
"Per a les llums intenses, empreu exactament el mateix mètode, I exactament "
"el mateix color, però establiu la capa a Divideix (la trobareu entre els "
"modes de barreja Aritmètic). Utilitzar Divideix té exactament l'efecte "
"contrari que utilitzar Multiplica amb el mateix color. El benefici d'això és "
"que podreu configurar amb facilitat una harmonia complementària entre les "
"vostres ombres i llums."

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:138
msgid ""
"Do this with all clusters and leaves, and maybe on the whole plant (you will "
"first need to stick it into a group layer given the background is opaque) "
"and you’re done!"
msgstr ""
"Feu això amb tots els grups i fulles, i potser a tota la planta (primer "
"haureu d'enganxar-la en una capa de grup atès que el fons és opac) i llest!"

#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:140
msgid ""
"Transform masks can be used on paint layers, vector layers, group layers, "
"clone layers and even file layers. I hope this tutorial has given you a nice "
"idea on how to use them, and hope to see much more use of the transform "
"masks in the future!"
msgstr ""
"Les màscares de transformació es poden utilitzar en capes de pintura, capes "
"vectorials, capes de grup, capes clonades i fins i tot capes de fitxer. "
"Espero que aquesta guia d'aprenentatge us hagi donat una bona idea sobre com "
"utilitzar-les, i en el futur espero veure molt més ús de les màscares de "
"transformació!"

# skip-rule: t-acc_obe
#: ../../tutorials/making_an_azalea_with_the_transformation_masks.rst:142
msgid ""
"You can get the file I made `here <https://share.kde.org/public.php?"
"service=files&t=48c601aaf17271d7ca516c44cbe8590e>`_ to examine it further! "
"(Caution: It will freeze up Krita if your version is below 2.9.4. The speed-"
"ups in 2.9.4 are due to this file.)"
msgstr ""
"`Aquí <https://share.kde.org/public.php?"
"service=files&t=48c601aaf17271d7ca516c44cbe8590e>`_ podreu fer-vos amb el "
"fitxer que vaig crear, per examinar-lo més a fons! (Precaució: el Krita es "
"congelarà si la seva versió és inferior a la 2.9.4. Les acceleracions en la "
"2.9.4 es deuen a aquest fitxer)."
