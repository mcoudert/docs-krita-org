# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:11+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita resourceworkspaces ref\n"

#: ../../<generated>:1
msgid "List of open documents."
msgstr "Lista dos documentos abertos."

#: ../../reference_manual/main_menu/window_menu.rst:1
msgid "The window menu in Krita."
msgstr "O menu da janela no Krita."

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "Window"
msgstr "Janela"

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "View"
msgstr "Ver"

#: ../../reference_manual/main_menu/window_menu.rst:16
msgid "Window Menu"
msgstr "O Menu Janela"

#: ../../reference_manual/main_menu/window_menu.rst:18
msgid "A menu completely dedicated to window management in Krita."
msgstr "Um menu completamente dedicado à gestão das janelas no Krita."

#: ../../reference_manual/main_menu/window_menu.rst:20
msgid "New Window"
msgstr "Nova Janela"

#: ../../reference_manual/main_menu/window_menu.rst:21
msgid "Creates a new window for Krita. Useful with multiple screens."
msgstr "Cria uma nova janela para o Krita. Útil com vários ecrãs."

#: ../../reference_manual/main_menu/window_menu.rst:22
msgid "New View"
msgstr "Nova Vista"

#: ../../reference_manual/main_menu/window_menu.rst:23
msgid ""
"Make a new view of the given document. You can have different zoom or "
"rotation on these."
msgstr ""
"Cria uma nova janela para o documento indicado. Poderá ter um nível de "
"ampliação ou rotação diferente nestes."

#: ../../reference_manual/main_menu/window_menu.rst:24
msgid "Workspace"
msgstr "Área de Trabalho"

#: ../../reference_manual/main_menu/window_menu.rst:25
msgid "A convenient access panel to the :ref:`resource_workspaces`."
msgstr "Um painel de acesso conveniente para os ref:`resource_workspaces`."

#: ../../reference_manual/main_menu/window_menu.rst:26
msgid "Close"
msgstr "Fechar"

#: ../../reference_manual/main_menu/window_menu.rst:27
msgid "Close the current view."
msgstr "Fecha a janela actual."

#: ../../reference_manual/main_menu/window_menu.rst:28
msgid "Close All"
msgstr "Fechar Tudo"

#: ../../reference_manual/main_menu/window_menu.rst:29
msgid "Close all documents."
msgstr "Fechar todos os documentos."

#: ../../reference_manual/main_menu/window_menu.rst:30
msgid "Tile"
msgstr "Lado-a-Lado"

#: ../../reference_manual/main_menu/window_menu.rst:31
msgid "Tiles all open documents into a little sub-window."
msgstr ""
"Coloca todos os documentos abertos lado-a-lado numa pequena sub-janela."

#: ../../reference_manual/main_menu/window_menu.rst:32
msgid "Cascade"
msgstr "Cascata"

#: ../../reference_manual/main_menu/window_menu.rst:33
msgid "Cascades the sub-windows."
msgstr "Coloca as sub-janelas em cascata."

#: ../../reference_manual/main_menu/window_menu.rst:34
msgid "Next"
msgstr "Seguinte"

#: ../../reference_manual/main_menu/window_menu.rst:35
msgid "Selects the next view."
msgstr "Selecciona a janela seguinte."

#: ../../reference_manual/main_menu/window_menu.rst:36
msgid "Previous"
msgstr "Anterior"

#: ../../reference_manual/main_menu/window_menu.rst:37
msgid "Selects the previous view."
msgstr "Selecciona a janela anterior."

#: ../../reference_manual/main_menu/window_menu.rst:39
msgid "Use this to switch between documents."
msgstr "Use isto para mudar entre documentos."
