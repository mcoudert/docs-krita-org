msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: gridsandguidesdocker icons Krita image Kritamouseleft\n"
"X-POFile-SpellExtra: toolgrid kbd gridtool Enter images alt ref mouseleft\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:48
msgid ""
".. image:: images/icons/grid_tool.svg\n"
"   :alt: toolgrid"
msgstr ""
".. image:: images/icons/grid_tool.svg\n"
"   :alt: grelha-ferramentas"

#: ../../reference_manual/tools/grid_tool.rst:1
msgid "Krita's grid tool reference."
msgstr "A referência à ferramenta da grelha do Krita."

#: ../../reference_manual/tools/grid_tool.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/grid_tool.rst:11
msgid "Grid"
msgstr "Grelha"

#: ../../reference_manual/tools/grid_tool.rst:16
msgid "Grid Tool"
msgstr "Ferramenta da Grelha"

#: ../../reference_manual/tools/grid_tool.rst:18
msgid "|toolgrid|"
msgstr "|toolgrid|"

#: ../../reference_manual/tools/grid_tool.rst:22
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""
"Descontinuado no 3.0; use como alternativa a :ref:`grids_and_guides_docker`."

#: ../../reference_manual/tools/grid_tool.rst:24
msgid ""
"When you click on the edit grid tool, you'll get a message saying that to "
"activate the grid you must press the :kbd:`Enter` key. Press the :kbd:"
"`Enter` key to make the grid visible. Now you must have noticed the tool "
"icon for your pointer has changed to icon similar to move tool."
msgstr ""
"Quando carregar na ferramenta para editar a grelha, irá obter uma mensagem "
"que lhe diz que para activar a grelha terá de carregar no :kbd:`Enter` para "
"tornar a grelha visível. Agora deverá ter notado que o ícone de ferramenta "
"do seu cursor mudou para um ícone semelhante ao da ferramenta para mover."

#: ../../reference_manual/tools/grid_tool.rst:27
msgid ""
"To change the spacing of the grid, press and hold the :kbd:`Ctrl` key and "
"then the |mouseleft| :kbd:`+ drag` shortcut on the canvas. In order to move "
"the grid you have to press the :kbd:`Alt` key and then the |mouseleft| :kbd:`"
"+ drag` shortcut."
msgstr ""
"Para mudar o intervalo da grelha, carregue e mantenha pressionado o :kbd:"
"`Ctrl` e depois o |mouseleft| + :kbd:`arrastamento` sobre a área de desenho. "
"Para mover a grelha, terá de carregar em :kbd:`Alt` e depois |mouseleft| + :"
"kbd:`arrastamento`."
