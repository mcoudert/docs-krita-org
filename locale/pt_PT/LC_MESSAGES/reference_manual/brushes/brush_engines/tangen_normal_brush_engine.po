# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-20 01:10+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en optionairbrush image optionratio optionsoftness\n"
"X-POFile-SpellExtra: tangentnormal images optionscatter optionsize\n"
"X-POFile-SpellExtra: optionspacing icons optionsharpness blendingmodes ref\n"
"X-POFile-SpellExtra: optionrotation menuselection optionbrushtip\n"
"X-POFile-SpellExtra: optiontexture optionmirror optionopacitynflow Krita\n"
"X-POFile-SpellExtra: Phong normals KritaFilterlayerinvertgreenchannel\n"
"X-POFile-SpellExtra: guilabel brushes\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_1.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_2.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ".. image:: images/brushes/Krita-normals-tutorial_3.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis Normal à Tangente."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "Motor de Pincéis Normal à Tangente"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr "Mapa da Normal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"O Motor de Pincéis da Normal à Tangente é um motor que está desenhado "
"especificamente para desenhar mapas da normal, da variedade da tangente. Por "
"seu turno, são usados em programas de 3D e em motores de jogos para tentar "
"fazer todos os truques de iluminação. Alguns usos comuns dos mapas da normal "
"incluem a falsificação de detalhes onde não existe e para lidar com as "
"transformações (Mapas de Fluxo)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"Um Mapa da Normal é uma imagem que contém informações de vectores. Em "
"particular, contêm informações sobre os Vectores da Normal, que são a "
"informação com a luz incide sobre uma superfície. Dado que os Vectores da "
"Normal são compostos por 3 coordenadas, como acontece com as cores, podemos "
"guardar e ver essa informação como cores."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"As normais podem ser consideradas semelhantes ao lápis na sua tablete. Como "
"tal, podemos usar os sensores de desvio que estão disponíveis em algumas "
"tabletes para gerar a cor das normais, que poderá depois ser usada por um "
"programa de 3D para criar efeitos de iluminação."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr "Em resumo, poderá pintar com superfícies em vez de cores."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr ""
"Estão disponíveis as seguintes opções no motor de pincéis normal à tangente:"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr "Parâmetros Específicos do Motor de Pincéis Normal à Tangente"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "Pressão à Tangente"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr ""
"Estas são as opções que definem como é que são calculadas as normais face "
"aos dados gerados pela tablete."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "Codificação da Tangente"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""
"Isto permite-lhe definir o que cada canal de cor significa. Os diferentes "
"programas definem diferentes coordenadas para diferentes canais, sendo uma "
"versão comum que o canal verde poderá ter de ser invertido (-Y) ou que o "
"canal verde esteja a guardar de facto o valor em X (+X)."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "Opções de Pressão"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr "Permite-lhe escolher qual o sensor que será usado para o X e o Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "Desvio"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr "Efectua um desvio tanto em X como em Y."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "Direcção"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""
"Usa o ângulo de desenho para o X e Y e a elevação do desvio para o Z. Isto "
"permite-lhe desenhar mapas de fluxo facilmente."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "Rotação"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""
"Usa a rotação para o X e o Y, e a elevação do desvio para o Z. Só está "
"disponível nas canetas especializadas."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "Sensibilidade da Elevação"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""
"Permite-lhe alterar o intervalo da normal que é gerado. Com 0, só irá pintar "
"na normal predefinida; com 1, irá pintar todas as normais num hemisfério "
"completo."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "Utilização"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""
"O Motor de Pincéis do Mapa da Normal é melhor usado com o Cursor de Desvio, "
"que pode ser definido em :menuselection:`Configuração --> Configurar o Krita "
"--> Geral --> Contorno da Forma --> Contorno do Desvio`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr "Fluxo de criação de Mapas da Normal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr ""
"Crie uma imagem com uma cor de fundo configurada como (128, 128, 255) - azul/"
"roxo."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr "Configurar um fundo com a cor predefinida."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""
"Configure o grupo com uma máscara de filtragem de Mapa de Relevo Phong. Use "
"a opção :guilabel:`Usar o mapa da normal` no filtro para que ele use as "
"normais."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr ""
"Crie uma camada de filtragem do mapa de relevo Phong; certifique-se que "
"assinala a opção 'Usar o mapa da normal'."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""
"Estas definições geram uma configuração de iluminação agradável de luz do "
"sol, com a luz 1 a ser o Sol, a luz 3 a ser a luz do céu e a luz 2 a ser a "
"luz do chão."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""
"Crie uma camada de filtragem :guilabel:`Normalizar` para normalizar o mapa "
"da normal antes de o passar ao mapa de relevo Phong para obter os melhores "
"resultados."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr "Depois, pinte as camadas no grupo para obter uma reacção directa."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""
"Pinte sobre a camada abaixo dos filtros com o pincel da normal à tangente "
"para que eles sejam convertidos em tempo-real."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""
"Finalmente, quando terminar, esconda a camada de filtragem do mapa de relevo "
"Phong (mas mantenha a camada de filtragem Normalizar!) e exporte o mapa da "
"normal para usar nos programas de 3D."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr "Desenhar Mapas da Direcção"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""
"Os mapas de direcções são criados com a opção :guilabel:`Direcção` nas "
"opções de :guilabel:`Desvio da Tangente`. Estes mapas da normal são usados "
"para distorcer as texturas num programa de 3D (para simular, por exemplo, o "
"fluxo da água) ou para criar mapas que indicam como o cabelo e o metal "
"escovado é raspado. O Krita não consegue de momento dar uma reacção de como "
"um dado mapa de direcções irá influenciar uma distorção ou um sombreado, mas "
"estes mapas são um pouco mais fáceis de ler."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""
"Basta configurar a opção de :guilabel:`Desvio da Tangente` para a :guilabel:"
"`Direcção` e desenhe. A direcção para a qual desenha o seu pincel será a "
"direcção que ficará codificada nas cores."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr "Só editar um único canal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""
"Algumas das vezes só irá querer editar um único canal. Nesse caso, defina o "
"modo de mistura do pincel para :guilabel:`Copiar o <canal>`, sendo que o "
"<canal> é substituído por vermelho, verde ou azul. Estes encontram-se na "
"secção :guilabel:`Diversos` dos modos de mistura."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""
"Por isso, se quiser que o pincel só afecte o canal vermelho, configure o "
"modo de mistura para :guilabel:`Copiar o vermelho`."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Os modos de mistura por cópia do vermelho, verde e azul também funcionam nas "
"camadas de filtragem."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""
"Isto também pode ser feito com as camadas de filtragem. Por isso, se quiser "
"inverter rapidamente o canal verde de uma camada, crie uma camada de "
"filtragem de inversão com o :guilabel:`Copiar o verde` por cima."

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr "Misturar os Mapas da Normal"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
"Para misturar dois mapas da normal, o Krita tem a opção do modo de mistura "
"ao :guilabel:`Combinar o Mapa da Normal` em :guilabel:`Diversos`."
