# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:22+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: trcsresult en image trcgraygradients images\n"
"X-POFile-SpellExtra: redgreenmixestrc srgbtrc Pepper ocm icc Basicreading\n"
"X-POFile-SpellExtra: uqe Lab colormanagementblending scRGB rec Response\n"
"X-POFile-SpellExtra: ICC TRC elle Peppertonecurves Gamma colorcategory\n"
"X-POFile-SpellExtra: Krita trcsv LAB Carrot sRGB EOTF Rec\n"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/trc_gray_gradients.svg"
msgstr ".. image:: images/color_category/trc_gray_gradients.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/Basicreading3trcsv2.svg"
msgstr ".. image:: images/color_category/Basicreading3trcsv2.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/red_green_mixes_trc.svg"
msgstr ".. image:: images/color_category/red_green_mixes_trc.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/3trcsresult.png"
msgstr ".. image:: images/color_category/3trcsresult.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:1
msgid "The effect of gamma and linear."
msgstr "O efeito do gama e do gama linear."

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Gamma"
msgstr "Gama"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear Color Space"
msgstr "Espaço de Cores Linear"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear"
msgstr "Linear"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Tone Response curve"
msgstr "Curva de Resposta dos Tons"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "EOTF"
msgstr "EOTF"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Transfer Curve"
msgstr "Curva de Transferência"

#: ../../general_concepts/colors/linear_and_gamma.rst:17
msgid "Gamma and Linear"
msgstr "Gama e Linear"

#: ../../general_concepts/colors/linear_and_gamma.rst:19
msgid ""
"Now, the situation we talk about when talking theory is what we would call "
"'linear'. Each step of brightness is the same value. Our eyes do not "
"perceive linearly. Rather, we find it more easy to distinguish between "
"darker grays than we do between lighter grays."
msgstr ""
"Agora, a situação que falamos na teoria é o que seria chamado de 'linear'. "
"Cada passo de brilho tem o mesmo valor. Os nossos olhos não têm uma "
"percepção linear. Em vez disso, é para nós mais fácil distinguir entre "
"cinzentos mais escuros do que entre os mais claros."

#: ../../general_concepts/colors/linear_and_gamma.rst:22
msgid ""
"As humans are the ones using computers, we have made it so that computers "
"will give more room to darker values in the coordinate system of the image. "
"We call this 'gamma-encoding', because it is applying a gamma function to "
"the TRC or transfer function of an image. The TRC in this case being the "
"Tone Response Curve or Tone Reproduction Curve or Transfer function (because "
"color management specialists hate themselves), which tells your computer or "
"printer how much color corresponds to a certain value."
msgstr ""
"Dado que os humanos são os utilizadores dos computadores, fez-se com que os "
"computadores dessem mais espaço aos valores escuros no sistema de "
"coordenadas da imagem. Chamamos a isto 'codificação do gama', porque está a "
"aplicar uma função-gama ao TRC ou função de transferência da imagem. O TRC "
"neste caso significa Tone Response Curve (Curva de Resposta das Tonalidades "
"ou Curva de Reprodução das Tonalidades), que indica ao seu computador ou "
"impressora quanta cor corresponde a um dado valor."

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ".. image:: images/color_category/Pepper_tonecurves.png"
msgstr ".. image:: images/color_category/Pepper_tonecurves.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ""
"One of the most common issues people have with Krita's color management is "
"the assigning of the right colorspace to the encoded TRC. Above, the center "
"Pepper is the right one, where the encoded and assigned TRC are the same. To "
"the left we have a Pepper encoded in sRGB, but assigned a linear profile, "
"and to the right we have a Pepper encoded with a linear TRC and assigned an "
"sRGB TRC. Image from `Pepper & Carrot <https://www.peppercarrot.com/>`_."
msgstr ""
"Uma das questões mais comuns que as pessoas têm com a gestão de cores do "
"Krita é a atribuição do espaço de cores correcto ao TRC codificado. Em cima, "
"a Pepper ao centro é a correcta, onde o TRC atribuído e a codificação do "
"mesmo são iguais. À esquerda, temos uma Pepper codificada em sRGB, mas com "
"um perfil linear atribuído, e à direita temos uma Pepper codificada com um "
"TRC linear e com um TRC sRGB atribuído. Imagem da `Pepper & Carrot <https://"
"www.peppercarrot.com/>`_."

#: ../../general_concepts/colors/linear_and_gamma.rst:30
msgid ""
"The following table shows how there's a lot of space being used by lighter "
"values in a linear space compared to the default sRGB TRC of our modern "
"computers and other TRCs available in our delivered profiles:"
msgstr ""
"A seguinte tabela mostra uqe existe bastante espaço usado pelos valores mais "
"claros num espaço linear, em comparação com o TRC predefinido do sRGB dos "
"nossos computadores recentes e e nos outros TRC's disponíveis com os nossos "
"perfis entregues:"

#: ../../general_concepts/colors/linear_and_gamma.rst:35
msgid ""
"If you look at linear of Rec. 709 TRCs, you can see there's quite a jump "
"between the darker shades and the lighter shades, while if we look at the "
"Lab L* TRC or the sRGB TRC, which seem more evenly spaced. This is due to "
"our eyes' sensitivity to darker values. This also means that if you do not "
"have enough bit depth, an image in a linear space will look as if it has "
"ugly banding. Hence why, when we make images for viewing on a screen, we "
"always use something like the Lab L\\*, sRGB or Gamma 2.2 TRCs to encode the "
"image with."
msgstr ""
"Se olhar para os TRC's da Rec. 709, poderá ver que existe um grande salto "
"entre os tons mais escuros e os mais claros, enquanto que se olharmos para o "
"TRC Lab L* ou para o do sRGB, aí estarão espaçados de forma mais uniforme. "
"Isto deve-se à sensibilidade dos nossos olhos aos valores mais escuros. Isto "
"também significa que se não tiver profundidade de cores suficiente, uma "
"imagem num espaço linear parecerá que tem algumas bandas feias. É por isso "
"que, quando criamos imagens para ver num ecrã, iremos sempre usar TRC's do "
"tipo do LAB L\\*, sRGB ou Gamma 2.2 a usar na codificação dos TRC's."

#: ../../general_concepts/colors/linear_and_gamma.rst:38
msgid ""
"However, this modification to give more space to darker values does lead to "
"wonky color maths when mixing the colors."
msgstr ""
"Contudo, esta modificação para dar mais espaço aos valores mais escuros gera "
"de facto contas matemáticas com cores mais estranhas quando se misturam as "
"cores."

#: ../../general_concepts/colors/linear_and_gamma.rst:40
msgid "We can see this with the following experiment:"
msgstr "Podemos ver isto com a seguinte experiência:"

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_1.png"
msgstr ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_1.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
"**Left:** Colored circles blurred in a regular sRGB space. **Right:** "
"Colored circles blurred in a linear space."
msgstr ""
"**Esquerda:** Círculos coloridos borrados num espaço RGB regular. **Direita:"
"** Círculos coloridos borrados num espaço linear."

#: ../../general_concepts/colors/linear_and_gamma.rst:48
msgid ""
"Colored circles, half blurred. In a gamma-corrected environment, this gives "
"an odd black border. In a linear environment, this gives us a nice gradation."
msgstr ""
"Círculos coloridos semi-borrados. Num ambiente com correcção do gama, isto "
"cria um contorno preto estranho. Num ambiente linear, gera uma graduação "
"correcta."

#: ../../general_concepts/colors/linear_and_gamma.rst:50
msgid "This also counts for Krita's color smudge brush:"
msgstr "Isto também se aplica no pincel de manchas a cores do Krita:"

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_2.png"
msgstr ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_2.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
"That's right, the 'muddying' of colors as is a common complaint by digital "
"painters everywhere, is in fact, a gamma-corrected colorspace mucking up "
"your colors. If you had been working in LAB to avoid this, be sure to try "
"out a linear rgb color space."
msgstr ""
"É isso mesmo, o aspecto 'lamacento' das cores é uma queixa comum dos "
"pintores digitais em todo o lado; de facto é um espaço de cores com "
"correcção do gama que está a 'destruir' as suas cores. Se esteve a trabalhar "
"no LAB para evitar isto, certifique-se que experimenta um espaço de cores "
"RGB linear."

#: ../../general_concepts/colors/linear_and_gamma.rst:59
msgid "What is happening under the hood"
msgstr "O que se passa nos bastidores"

#: ../../general_concepts/colors/linear_and_gamma.rst:62
msgid "Imagine we want to mix red and green."
msgstr "Imagine que desejamos misturar o vermelho com verde."

#: ../../general_concepts/colors/linear_and_gamma.rst:64
msgid ""
"First, we would need the color coordinates of red and green inside our color "
"space's color model. So, that'd be..."
msgstr ""
"Em primeiro lugar, iremos precisar das coordenadas de cores do vermelho e do "
"verde no nosso espaço de cores. Como tal, isso seria..."

#: ../../general_concepts/colors/linear_and_gamma.rst:67
msgid "Color"
msgstr "Cor"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:78
msgid "Red"
msgstr "Vermelho"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "Green"
msgstr "Verde"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "Blue"
msgstr "Azul"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "1.0"
msgstr "1,0"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "0.0"
msgstr "0,0"

#: ../../general_concepts/colors/linear_and_gamma.rst:73
msgid "We then average these coordinates over three mixes:"
msgstr "Depois iremos misturar essas coordenadas com três misturas:"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix1"
msgstr "Mistura1"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix2"
msgstr "Mistura2"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix3"
msgstr "Mistura3"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.75"
msgstr "0,75"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.5"
msgstr "0,5"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.25"
msgstr "0,25"

#: ../../general_concepts/colors/linear_and_gamma.rst:85
msgid ""
"But to figure out how these colors look on screen, we first put the "
"individual values through the TRC of the color-space we're working with:"
msgstr ""
"Mas para descobrir como ficam essas cores no ecrã, são colocados primeiro os "
"valores individuais no TRC do espaço de cores com que trabalhamos:"

#: ../../general_concepts/colors/linear_and_gamma.rst:93
msgid ""
"Then we fill in the values into the correct spot. Compare these to the "
"values of the mixture table above!"
msgstr ""
"Depois preenchemos os valores no local correcto. Compare estes valores com "
"os da tabela de misturas acima!"

#: ../../general_concepts/colors/linear_and_gamma.rst:99
msgid ""
"And this is why color mixtures are lighter and softer in linear space. "
"Linear space is more physically correct, but sRGB is more efficient in terms "
"of space, so hence why many images have an sRGB TRC encoded into them. In "
"case this still doesn't make sense: *sRGB gives largely* **darker** *values "
"than linear space for the same coordinates*."
msgstr ""
"E é por isto que as misturas de cores são mais claras e suaves num espaço "
"linear. O espaço linear é fisicamente mais correcto, mas o sRGB é mais "
"eficiente em termos de espaço, e é por isso que tantas imagens têm um TRC "
"sRGB codificado nelas. No caso de isto ainda não fazer sentido: *o sRGB gera "
"valores bastante* **mais escuros** *que o espaço linear para as mesmas "
"coordenadas*."

#: ../../general_concepts/colors/linear_and_gamma.rst:102
msgid ""
"So different TRCs give different mixes between colors, in the following "
"example, every set of gradients is in order a mix using linear TRC, a mix "
"using sRGB TRC and a mix using Lab L* TRC."
msgstr ""
"Como tal, diferentes TRC's geram diferentes misturas entre cores. No "
"seguinte exemplo, cada um dos conjuntos de gradientes é, respectivamente uma "
"mistura com TRC linear, uma mistura com o TRC do sRGB e uma mistura com o "
"TRC do LAB L*."

#: ../../general_concepts/colors/linear_and_gamma.rst:110
msgid ""
"So, you might be asking, how do I tick this option? Is it in the settings "
"somewhere? The answer is that we have several ICC profiles that can be used "
"for this kind of work:"
msgstr ""
"Como tal, poderá perguntar: como é que assinalo esta opção? Está algures na "
"configuração? A resposta é que temos diversos perfis ICC que podem ser "
"usados para este tipo de trabalho:"

#: ../../general_concepts/colors/linear_and_gamma.rst:112
msgid "scRGB (linear)"
msgstr "scRGB (linear)"

#: ../../general_concepts/colors/linear_and_gamma.rst:113
msgid "All 'elle'-profiles ending in 'g10', such as *sRGB-elle-v2-g10.icc*."
msgstr ""
"Todos os perfis 'elle' que terminam em 'g10', como o *sRGB-elle-v2-g10.icc*."

#: ../../general_concepts/colors/linear_and_gamma.rst:115
msgid ""
"In fact, in all the 'elle'-profiles, the last number indicates the gamma. "
"1.0 is linear, higher is gamma-corrected and 'srgbtrc' is a special gamma "
"correction for the original sRGB profile."
msgstr ""
"De facto, em todos os perfis 'elle', o último número indica o gama. O 1,0 é "
"o linear, os valores superiores têm correcções de gama e o 'srgbtrc' é uma "
"correcção especial do gama para o perfil sRGB original."

#: ../../general_concepts/colors/linear_and_gamma.rst:117
msgid ""
"If you use the color space browser, you can tell the TRC from the 'estimated "
"gamma'(if it's 1.0, it's linear), or from the TRC widget in Krita 3.0, which "
"looks exactly like the curve graphs above."
msgstr ""
"Se usar o navegador de espaços de cores, poderá definir o TRC a partir do "
"'gama estimado' (se for igual a 1,0, é linear), ou do elemento do TRC no "
"Krita 3.0, que se parece exactamente com os gráficos das curvas acima."

#: ../../general_concepts/colors/linear_and_gamma.rst:119
msgid ""
"Even if you do not paint much, but are for example making textures for a "
"videogame or rendering, using a linear space is very beneficial and will "
"speed up the renderer a little, for it won't have to convert images on its "
"own."
msgstr ""
"Mesmo se não pintar muito, mas se criar por exemplo texturas para jogos ou "
"cenários, o uso de um espaço linear é bastante benéfico e irá acelerar um "
"pouco o desenho dos mesmos, já que não terá de converter por si só as "
"imagens."

#: ../../general_concepts/colors/linear_and_gamma.rst:121
msgid ""
"The downside of linear space is of course that white seems very overpowered "
"when mixing with black, because in a linear space, light grays get more "
"room. In the end, while linear space is physically correct, and a boon to "
"work in when you are dealing with physically correct renderers for "
"videogames and raytracing, Krita is a tool and no-one will hunt you down for "
"preferring the dark mixing of the sRGB TRC."
msgstr ""
"A desvantagem óbvia do espaço linear é que o branco parece muito menos forte "
"quando o misturar com o preto, porque num espaço linear, os cinzentos claros "
"ocupam mais espaço. Por fim, embora o espaço linear seja fisicamente "
"correcto e seja fácil de trabalhar com ele quando lidar com os sistemas de "
"desenho de jogos e desenho artístico, o Krita é uma ferramenta e ninguém o "
"julgará por preferir a mistura mais escura do TRC do sRGB."
