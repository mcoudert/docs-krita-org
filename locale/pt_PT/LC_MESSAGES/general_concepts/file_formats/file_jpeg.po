# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-21 13:29+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: co Krita jpg filegif YCrCb filepng senos ref\n"

#: ../../general_concepts/file_formats/file_jpeg.rst:1
msgid "The JPEG file format as exported by Krita."
msgstr "O formato de ficheiros JPEG, tal como exportado pelo Krita."

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpeg"
msgstr "jpeg"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpg"
msgstr "jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "*.jpg"
msgstr "*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:16
msgid "\\*.jpg"
msgstr "\\*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:18
msgid ""
"``.jpg``, ``.jpeg`` or ``.jpeg2000`` are a family of file-formats designed "
"to encode photographs."
msgstr ""
"Os formatos ``.jpg``, ``.jpeg`` ou ``.jpeg2000`` são uma família de formatos "
"de ficheiros desenhados para codificar fotografias."

#: ../../general_concepts/file_formats/file_jpeg.rst:20
msgid ""
"Photographs have the problem that they have a lot of little gradients, which "
"means that you cannot index the file like you can with :ref:`file_gif` and "
"expect the result to look good. What JPEG instead does is that it converts "
"the file to a perceptual color space (:ref:`YCrCb <model_ycrcb>`), and then "
"compresses the channels that encode the colors, while keeping the channel "
"that holds information about the relative lightness uncompressed. This works "
"really well because human eye-sight is not as sensitive to colorfulness as "
"it is to relative lightness. JPEG also uses other :ref:`lossy "
"<lossy_compression>` compression techniques, like using cosine waves to "
"describe image contrasts."
msgstr ""
"As fotografias têm o problema que têm uma grande quantidade de pequenos "
"gradientes, o que significa que não consegue indexar o ficheiro, como faria "
"com o :ref:`file_gif`, e esperar que o resultado ficasse bom. O que o JPEG "
"faz, por sua vez, é converter o ficheiro para o espaço de cores por "
"percepção (:ref:`YCrCb <model_ycrcb>`) e depois comprime os canais que "
"codificam as cores, mantendo à mesma o canal que contém a informação sobre a "
"luminosidade relativa de forma não comprimida. Isto funciona realmente bem, "
"porque o olho humano não é tão sensível às cores como é para a luminosidade "
"relativa. O JPEG também usa outras técnicas de compressão :ref:`com perdas "
"<lossy_compression>`, como o uso de ondas de co-senos para descrever os "
"contrastes da imagem."

#: ../../general_concepts/file_formats/file_jpeg.rst:22
msgid ""
"However, it does mean that JPEG should be used in certain cases. For images "
"with a lot of gradients, like full scale paintings, JPEG performs better "
"than :ref:`file_png` and :ref:`file_gif`."
msgstr ""
"Contudo, isto significa que o JPEG deve ser usado em certos casos. Para "
"imagens com bastantes gradientes, como as pinturas em grande escala, o JPEG "
"comporta-se melhor que o :ref:`file_png` e o :ref:`file_gif`."

#: ../../general_concepts/file_formats/file_jpeg.rst:24
msgid ""
"But for images with a lot of sharp contrasts, like text and comic book "
"styles, PNG is a much better choice despite a larger file size. For "
"grayscale images, :ref:`file_png` and :ref:`file_gif` will definitely be "
"more efficient."
msgstr ""
"Mas para as imagens com contrastes bem definidos, como o texto ou estilos de "
"bandas desenhadas, o PNG é uma escolha muito melhor, apesar de gerar um "
"tamanho maior dos ficheiros. Para as imagens em tons de cinzento, o :ref:"
"`file_png` e o :ref:`file_gif` serão decerto mais eficientes."

#: ../../general_concepts/file_formats/file_jpeg.rst:26
msgid ""
"Because JPEG uses lossy compression, it is not advised to save over the same "
"JPEG multiple times. The lossy compression will cause the file to reduce in "
"quality each time you save it. This is a fundamental problem with lossy "
"compression methods. Instead use a lossless file format, or a working file "
"format while you are working on the image."
msgstr ""
"Como o JPEG usa compressão com perdas, não é aconselhado para gravar sobre o "
"mesmo ficheiro várias vezes. A compressão com perdas fará com que o ficheiro "
"perca qualidade de cada vez que o grava. Isto é um problema fundamental com "
"os métodos de compressão com perdas. Em vez disso, use um formato de "
"compressão sem perdas ou um formato de ficheiros mais funcional, enquanto "
"estiver a trabalhar sobre a imagem."
