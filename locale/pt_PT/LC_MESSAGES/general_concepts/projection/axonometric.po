# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:19+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en projection projectionimage image menuselection kbd\n"
"X-POFile-SpellExtra: categoryprojection images axonométrica dimétrica\n"
"X-POFile-SpellExtra: gif guilabel ref pô projectionanimation Krita\n"
"X-POFile-SpellExtra: Inkscape Axonométrica Dimétrica\n"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_07.svg"
msgstr ".. image:: images/category_projection/projection-cube_07.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_08.svg"
msgstr ".. image:: images/category_projection/projection-cube_08.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_10.svg"
msgstr ".. image:: images/category_projection/projection-cube_10.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_11.svg"
msgstr ".. image:: images/category_projection/projection-cube_11.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_15.png"
msgstr ".. image:: images/category_projection/projection_image_15.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_16.png"
msgstr ".. image:: images/category_projection/projection_image_16.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_17.png"
msgstr ".. image:: images/category_projection/projection_image_17.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_18.png"
msgstr ".. image:: images/category_projection/projection_image_18.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_19.png"
msgstr ".. image:: images/category_projection/projection_image_19.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_20.png"
msgstr ".. image:: images/category_projection/projection_image_20.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_21.png"
msgstr ".. image:: images/category_projection/projection_image_21.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_22.png"
msgstr ".. image:: images/category_projection/projection_image_22.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_23.png"
msgstr ".. image:: images/category_projection/projection_image_23.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_24.png"
msgstr ".. image:: images/category_projection/projection_image_24.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_25.png"
msgstr ".. image:: images/category_projection/projection_image_25.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_26.png"
msgstr ".. image:: images/category_projection/projection_image_26.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_27.png"
msgstr ".. image:: images/category_projection/projection_image_27.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_28.png"
msgstr ".. image:: images/category_projection/projection_image_28.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_29.png"
msgstr ".. image:: images/category_projection/projection_image_29.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_animation_02.gif"
msgstr ".. image:: images/category_projection/projection_animation_02.gif"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_30.png"
msgstr ".. image:: images/category_projection/projection_image_30.png"

#: ../../general_concepts/projection/axonometric.rst:1
msgid "Axonometric projection."
msgstr "Projecção axonométrica."

#: ../../general_concepts/projection/axonometric.rst:10
msgid ""
"This is a continuation of :ref:`the orthographic and oblique tutorial "
"<projection_orthographic>`, be sure to check it out if you get confused!"
msgstr ""
"Esta é uma continuação do :ref:`tutorial sobre projecções ortográficas e "
"oblíquas <projection_orthographic>`; certifique-se que os consulta se "
"estiver confuso!"

#: ../../general_concepts/projection/axonometric.rst:12
#: ../../general_concepts/projection/axonometric.rst:16
msgid "Axonometric"
msgstr "Axonométrica"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Projection"
msgstr "Projecção"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Dimetric"
msgstr "Dimétrica"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Isometric"
msgstr "Isométrica"

#: ../../general_concepts/projection/axonometric.rst:18
msgid "So, the logic of adding the top is still similar to that of the side."
msgstr ""
"Como tal, a lógica de adicionar o topo é ainda muito semelhante à do lado."

#: ../../general_concepts/projection/axonometric.rst:23
msgid ""
"Not very interesting. But it gets much more interesting when we use a side "
"projection:"
msgstr ""
"Não é muito interessante. Mas ficará muito mais interessante quando usarmos "
"uma projecção lateral:"

#: ../../general_concepts/projection/axonometric.rst:28
msgid ""
"Because our cube is red on both front-sides, and blue on both left and right "
"side, we can just use copies, this simplifies the method for cubes a lot. We "
"call this form of axonometric projection 'dimetric' as it deforms two "
"parallel lines equally."
msgstr ""
"Dado que o nosso cubo é vermelho, em ambas as vistas frontais, e azul nos "
"lados esquerdo e direito, podemos simplesmente usar cópias; isto simplifica "
"bastante o método para os cubos. Chamamos a esta forma de projecção "
"axonométrica 'dimétrica' porque deforma as duas linhas paralelas de igual "
"forma."

#: ../../general_concepts/projection/axonometric.rst:30
msgid ""
"Isometric is sorta like dimetric where we have the same angle between all "
"main lines:"
msgstr ""
"A isométrica é um pouco como a dimétrica, onde temos o mesmo ângulo entre "
"todas as linhas principais:"

#: ../../general_concepts/projection/axonometric.rst:35
msgid ""
"True isometric is done with a 90-54.736=35.264° angle from ground plane:"
msgstr ""
"A isométrica verdadeira é feita com um ângulo  90-54,736=35,264° a partir do "
"plano do chão:"

#: ../../general_concepts/projection/axonometric.rst:40
msgid ""
"(as you can see, it doesn't line up perfectly, because Inkscape, while more "
"designed for making these kinds of diagrams than Krita, doesn't have tools "
"to manipulate the line's angle in degrees)"
msgstr ""
"(como pode ver, não se alinha perfeitamente, dado que o Inkscape, embora "
"mais preparado para criar estes tipos de diagramas que o Krita, não tem "
"ferramentas para manipular o ângulo da linha em graus)"

#: ../../general_concepts/projection/axonometric.rst:42
msgid ""
"This is a bit of an awkward angle, and on top of that, it doesn't line up "
"with pixels sensibly, so for videogames an angle of 30° from the ground "
"plane is used."
msgstr ""
"Este é um ângulo um pouco estranho e, para além disso, não se alinha "
"sensivelmente com os pixels; por isso, para os jogos de vídeo, é usado um "
"ângulo de 30° a partir do plano do chão."

#: ../../general_concepts/projection/axonometric.rst:47
msgid "Alright, so, let's make an isometric out of our boy then."
msgstr "Muito bem, então agora vamos criar uma isométrica do rapaz."

#: ../../general_concepts/projection/axonometric.rst:49
msgid "We make a new document, and add a vector layer."
msgstr "Iremos criar um novo documento e adicionar uma camada vectorial."

#: ../../general_concepts/projection/axonometric.rst:51
msgid ""
"On the vector layer, we select the straight line tool, start a line and then "
"hold the :kbd:`Shift` key to make it snap to angles. This'll allow us to "
"make a 30° setup like above:"
msgstr ""
"Na camada vectorial, iremos seleccionar a ferramenta da linha recta, iniciar "
"uma linha e depois manter carregado o :kbd:`Shift` para que ela se ajuste "
"aos ângulos. Isto irá possibilitar a configuração em 30° como foi feito "
"acima:"

#: ../../general_concepts/projection/axonometric.rst:56
msgid ""
"We then import some of the frames from the animation via :menuselection:"
"`Layers --> Import/Export --> Import layer`."
msgstr ""
"Iremos então importar algumas das imagens da animação através da opção :"
"menuselection:`Camadas --> Importar/Exportar --> Importar uma camada`."

#: ../../general_concepts/projection/axonometric.rst:58
msgid ""
"Then crop it by setting the crop tool to :guilabel:`Layer`, and use :"
"menuselection:`Filters --> Colors --> Color to alpha` to remove any "
"background. I also set the layers to 50% opacity. We then align the vectors "
"to them:"
msgstr ""
"Depois iremos recortá-la, seleccionando a ferramenta de recorte como :"
"guilabel:`Camada` e usando os :menuselection:`Filtros --> Cores --> Cor para "
"o alfa` para remover todo o fundo. Também foi configura a opacidade a 50%. "
"Finalmente, alinham-se os vectores a eles:"

#: ../../general_concepts/projection/axonometric.rst:65
msgid ""
"To resize a vector but keep its angle, you just select it with the shape "
"handling tool (the white arrow) drag on the corners of the bounding box to "
"start moving them, and then press the :kbd:`Shift` key to constrain the "
"ratio. This'll allow you to keep the angle."
msgstr ""
"Para dimensionar um vector mas mantendo o seu ângulo, podê-lo-á seleccionar "
"com a ferramenta de tratamento de formas - a seta branca - arraste os cantos "
"da área envolvente para os começar a mover e depois carregue em :kbd:`Shift` "
"para restringir as proporções. Isto permitir-lhe-á manter o ângulo."

#: ../../general_concepts/projection/axonometric.rst:67
msgid ""
"The lower image is 'the back seen from the front', we'll be using this to "
"determine where the ear should go."
msgstr ""
"A imagem inferior é 'a parte de trás vista de frente' e será usada para "
"determinar onde irá ficar a orelha."

#: ../../general_concepts/projection/axonometric.rst:69
msgid ""
"Now, we obviously have too little space, so select the crop tool, select :"
"guilabel:`Image` and tick :guilabel:`Grow` and do the following:"
msgstr ""
"Agora, obviamente teremos pouco espaço, por isso seleccione a ferramenta de "
"recorte, seleccione a :guilabel:`Imagem` e assinale a opção :guilabel:"
"`Aumentar` e depois o seguinte:"

#: ../../general_concepts/projection/axonometric.rst:74
msgid ""
"Grow is a more practical way of resizing the canvas in width and height "
"immediately."
msgstr ""
"O Aumento é uma forma mais prática de dimensionar imediatamente a área de "
"desenho na sua altura e largura."

#: ../../general_concepts/projection/axonometric.rst:76
msgid ""
"Then we align the other heads and transform them by using the transform tool "
"options:"
msgstr ""
"Depois iremos alinhar as outras cabeças e transformá-las com as opções da "
"ferramenta de transformação:"

#: ../../general_concepts/projection/axonometric.rst:81
msgid "(330° here is 360°-30°)"
msgstr "(os 330° aqui são 360°-30°)"

#: ../../general_concepts/projection/axonometric.rst:83
msgid ""
"Our rectangle we'll be working in slowly becomes visible. Now, this is a bit "
"of a difficult angle to work at, so we go to :menuselection:`Image --> "
"Rotate --> Rotate Image` and fill in 30° clockwise:"
msgstr ""
"O nosso rectângulo que estaremos a tratar lentamente vai ficando visível. "
"Agora, este é um ângulo um pouco difícil de usar, por isso vamos a :"
"menuselection:`Imagem --> Rodar --> Rotação Personalizada` e preencher 30° "
"no sentido horário (dos ponteiros do relógio):"

#: ../../general_concepts/projection/axonometric.rst:90
msgid ""
"(of course, we could've just rotated the left two images 30°, this is mostly "
"to be less confusing compared to the cube)"
msgstr ""
"(obviamente poderíamos ter simplesmente rodado as duas imagens da esquerda "
"em 30°; isto é principalmente para ser menos confuso em comparação com o "
"cubo)"

#: ../../general_concepts/projection/axonometric.rst:92
msgid ""
"So, we do some cropping, some cleanup and add two parallel assistants like "
"we did with the orthographic:"
msgstr ""
"Como tal, fizemos alguns recortes, algumas limpezas e adicionámos dois "
"assistentes paralelos como fizemos com a projecção ortográfica:"

#: ../../general_concepts/projection/axonometric.rst:97
msgid ""
"So the idea here is that you draw parallel lines from both sides to find "
"points in the drawing area. You can use the previews of the assistants for "
"this to keep things clean, but I drew the lines anyway for your convenience."
msgstr ""
"Como tal, a ideia aqui é que desenhe linhas paralelas de ambos os lados para "
"encontrar pontos na área de desenho. Poderá usar as antevisões dos "
"assistentes para manter as coisas arrumadas; contudo, foram desenhadas as "
"linhas à mesma para sua conveniência."

#: ../../general_concepts/projection/axonometric.rst:102
msgid ""
"The best is to make a few sampling points, like with the eyebrows here, and "
"then draw the eyebrow over it."
msgstr ""
"O melhor é criar alguns pontos de amostragem, como as sobrancelhas, e depois "
"desenhar a sobrancelha sobre eles."

#: ../../general_concepts/projection/axonometric.rst:108
msgid "Alternative axonometric with the transform tool"
msgstr "Axonométrica alternativa com a ferramenta de transformação"

#: ../../general_concepts/projection/axonometric.rst:110
msgid ""
"Now, there's an alternative way of getting there that doesn't require as "
"much space."
msgstr ""
"Agora, existe uma forma alternativa de chegar aqui, que não ocupa muito "
"espaço."

#: ../../general_concepts/projection/axonometric.rst:112
msgid ""
"We open our orthographic with :guilabel:`Open existing Document as Untitled "
"Document` so that we don't save over it."
msgstr ""
"Iremos abrir a nossa projecção ortográfica com :guilabel:`Abrir um Documento "
"Existente como um Documento sem Título` para não gravemos sobre ela."

#: ../../general_concepts/projection/axonometric.rst:114
msgid ""
"Our game-safe isometric has its angle at two pixels horizontal is one pixel "
"vertical. So, we shear the ortho graphics with transform masks to -.5/+.5 "
"pixels (this is proportional)"
msgstr ""
"A nossa isométrica adequada para jogos tem o seu ângulo em dois pixels "
"horizontais e um vertical. Como tal, iremos inclinar as imagens ortográficas "
"com máscaras de transformação iguais a -0,5/+0,5 pixels (isto é proporcional)"

#: ../../general_concepts/projection/axonometric.rst:119
msgid ""
"Use the grid to setup two parallel rulers that represent both diagonals (you "
"can snap them with the :kbd:`Shift + S` shortcut):"
msgstr ""
"Use a grelha para configurar duas réguas paralelas que representam ambas as "
"diagonais (podê-las-á ajustar com a combinação :kbd:`Shift + S`):"

#: ../../general_concepts/projection/axonometric.rst:124
msgid "Add the top view as well:"
msgstr "Adicione também a vista de topo:"

#: ../../general_concepts/projection/axonometric.rst:129
msgid "if you do this for all slices, you get something like this:"
msgstr "se o fizer para todas as fatias, irá obter algo do género:"

#: ../../general_concepts/projection/axonometric.rst:134
msgid ""
"Using the parallel rulers, you can then figure out the position of a point "
"in 3d-ish space:"
msgstr ""
"Usando as réguas paralelas, poderá descobrir a posição de um dado ponto num "
"espaço 3D:"

#: ../../general_concepts/projection/axonometric.rst:139
msgid "As you can see, this version both looks more 3d as well as more creepy."
msgstr ""
"Como poderá ver, esta versão parece-me mais com uma versão 3D, assim como "
"mais assustadora também."

#: ../../general_concepts/projection/axonometric.rst:141
msgid ""
"That's because there are less steps involved as the previous version -- "
"We're deriving our image directly from the orthographic view -- so there are "
"less errors involved."
msgstr ""
"Isto acontece porque existe menos passos envolvidos que na versão anterior "
"-- iremos derivar a nossa imagem directamente da vista ortográfica -- como "
"tal, existem menos erros envolvidos."

#: ../../general_concepts/projection/axonometric.rst:143
msgid ""
"The creepiness is because we've had the tiniest bit of stylisation in our "
"side view, so the eyes come out HUGE. This is because when we stylize the "
"side view of an eye, we tend to draw it not perfectly from the side, but "
"rather slightly at an angle. If you look carefully at the turntable, the "
"same problem crops up there as well."
msgstr ""
"O susto é derivado de termos aplicado algum estilo na nossa vista lateral, "
"pelo que os olhos agora saem ENORMES. Isto acontece porque, quando "
"estilizamos a vista lateral de um dado olho, tendemos a desenhá-lo não de "
"forma perfeita de um dado lado, mas sim a partir de um dado ângulo. Se olhar "
"com cuidado para a vista rotativa, o mesmo problema acontece aqui também."

#: ../../general_concepts/projection/axonometric.rst:145
msgid ""
"Generally, stylized stuff tends to fall apart in 3d view, and you might need "
"to make some choices on how to make it work."
msgstr ""
"De um modo geral, as coisas estilizadas tendem a ficar horríveis numa vista "
"3D, pelo que poderá ter de fazer algumas escolhas sobre como pô-las a "
"funcionar."

#: ../../general_concepts/projection/axonometric.rst:147
msgid ""
"For example, we can just easily fix the side view (because we used transform "
"masks, this is easy.)"
msgstr ""
"Por exemplo, poderá simplesmente corrigir a vista lateral (como usámos "
"máscaras de transformação, isto é simples.)"

#: ../../general_concepts/projection/axonometric.rst:152
msgid "And then generate a new drawing from that…"
msgstr "E depois gerar um novo desenho a partir daí…"

#: ../../general_concepts/projection/axonometric.rst:157
msgid ""
"Compare to the old one and you should be able to see that the new result’s "
"eyes are much less creepy:"
msgstr ""
"Compare com o antigo e poderá ver que os olhos do novo resultado parecem "
"muito menos assustadores:"

#: ../../general_concepts/projection/axonometric.rst:162
msgid ""
"It still feels very squashed compared to the regular parallel projection "
"above, and it might be an idea to not just skew but also stretch the orthos "
"a bit."
msgstr ""
"Ainda parece um pouco esmagado em comparação com a projecção paralela normal "
"acima, e poderá ser uma ideia não apenas inclinar, mas também esticar um "
"pouco os pontos."

#: ../../general_concepts/projection/axonometric.rst:164
msgid "Let's continue with perspective projection in the next one!"
msgstr "Iremos continuar com a projecção em perspectiva no próximo!"
