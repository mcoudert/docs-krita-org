# Spanish translations for docs_krita_org_reference_manual___preferences___color_selector_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___color_selector_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-04 01:37+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Make Brush Color Bluer"
msgstr "Hacer el color del pincel más azul"

#: ../../reference_manual/preferences/color_selector_settings.rst:1
msgid "The color selector settings in Krita."
msgstr "Las preferencias del selector de color en Krita."

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
#, fuzzy
#| msgid "Color Selector Settings"
msgid "Color Selector"
msgstr "Preferencias del selector de color"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:16
msgid "Color Selector Settings"
msgstr "Preferencias del selector de color"

#: ../../reference_manual/preferences/color_selector_settings.rst:18
msgid ""
"These settings directly affect Advanced Color Selector Dockers and the same "
"dialog box appears when the user clicks the settings button in that docker "
"as well. They also affect certain hotkey actions."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:20
msgid ""
"This settings menu has a drop-down for Advanced Color Selector, and Color "
"Hotkeys."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:23
msgid "Advanced Color Selector"
msgstr "Selector de color avanzado"

#: ../../reference_manual/preferences/color_selector_settings.rst:25
msgid ""
"These settings are described on the page for the :ref:"
"`advanced_color_selector_docker`."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:28
msgid "Color Hotkeys"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:30
msgid "These allow you to set the steps for the following actions:"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:32
msgid "Make Brush Color Darker"
msgstr "Hacer el color del pincel más oscuro"

#: ../../reference_manual/preferences/color_selector_settings.rst:33
msgid ""
"This is defaultly set to :kbd:`K` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:34
msgid "Make Brush Color Lighter"
msgstr "Hacer el color del pincel más claro"

#: ../../reference_manual/preferences/color_selector_settings.rst:35
msgid ""
"This is defaultly set to :kbd:`L` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:36
msgid "Make Brush Color More Saturated"
msgstr "Hacer el color del pincel más saturado"

#: ../../reference_manual/preferences/color_selector_settings.rst:37
#: ../../reference_manual/preferences/color_selector_settings.rst:39
msgid "This is defaultly unset and uses the :guilabel:`saturation` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:38
msgid "Make Brush Color More Desaturated"
msgstr "Hacer el color del pincel más desaturado"

#: ../../reference_manual/preferences/color_selector_settings.rst:40
msgid "Shift Brushcolor Hue clockwise"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:41
#: ../../reference_manual/preferences/color_selector_settings.rst:43
msgid "This is defaultly unset and uses the :guilabel:`Hue` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:42
msgid "Shift Brushcolor Hue counter-clockwise"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:44
msgid "Make Brush Color Redder"
msgstr "Hacer el color del pincel más rojo"

#: ../../reference_manual/preferences/color_selector_settings.rst:45
#: ../../reference_manual/preferences/color_selector_settings.rst:47
msgid "This is defaultly unset and uses the :guilabel:`Redder/Greener` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:46
msgid "Make Brush Color Greener"
msgstr "Hacer el color del pincel más verde"

#: ../../reference_manual/preferences/color_selector_settings.rst:48
msgid "Make Brush Color Yellower"
msgstr "Hacer el color del pincel más amarillo"

#: ../../reference_manual/preferences/color_selector_settings.rst:49
#: ../../reference_manual/preferences/color_selector_settings.rst:51
msgid "This is defaultly unset and uses the :guilabel:`Bluer/Yellower` steps."
msgstr ""
