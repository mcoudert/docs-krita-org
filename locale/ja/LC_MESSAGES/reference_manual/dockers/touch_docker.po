msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/dockers/touch_docker.rst:1
msgid "Overview of the touch docker."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Touch"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Finger"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Tablet UI"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:15
msgid "Touch Docker"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:17
msgid ""
"The Touch Docker is a QML docker with several convenient actions on it. Its "
"purpose is to aid those who use Krita on a touch-enabled screen by providing "
"bigger gui elements."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:19
msgid "Its actions are..."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Open File"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save File"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save As"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Undo"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Redo"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:26
msgid "Decrease Opacity"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:28
msgid "Increase Opacity"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:30
msgid "Increase Lightness"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:32
msgid "Decrease Lightness"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:34
msgid "Zoom in"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Counter Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Reset Canvas Rotation"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:38
msgid "Zoom out"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:40
msgid "Decrease Brush Size"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:42
msgid "Increase Brush Size"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:44
msgid "Delete Layer Contents"
msgstr ""
