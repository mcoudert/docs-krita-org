# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:19+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: frihandspenselverktyg"

#: ../../reference_manual/tools/freehand_brush.rst:1
msgid ""
"Krita's freehand brush tool reference, containing how to use the stabilizer "
"in krita."
msgstr ""
"Referens för Kritas frihandspenselverktyg, som innehåller hur stabilisatorn "
"används i Krita."

#: ../../reference_manual/tools/freehand_brush.rst:12
msgid "Freehand Brush"
msgstr "Frihandspensel"

#: ../../reference_manual/tools/freehand_brush.rst:12
msgid "Freehand"
msgstr "Frihand"

#: ../../reference_manual/tools/freehand_brush.rst:17
msgid "Freehand Brush Tool"
msgstr "Frihandspenselverktyg"

#: ../../reference_manual/tools/freehand_brush.rst:19
msgid "|toolfreehandbrush|"
msgstr "|toolfreehandbrush|"

#: ../../reference_manual/tools/freehand_brush.rst:21
msgid ""
"The default tool you have selected on Krita start-up, and likely the tool "
"that you will use the most."
msgstr ""
"Standardverktyget som är valt när Krita startas, och troligen det verktyg "
"som man använder allra mest."

#: ../../reference_manual/tools/freehand_brush.rst:23
msgid ""
"The freehand brush tool allows you to paint on paint layers without "
"constraints like the straight line tool. It makes optimal use of your "
"tablet's input settings to control the brush-appearance. To switch the "
"brush, make use of the brush-preset docker."
msgstr ""
"Frihandspenselverktyget låter dig måla på målarlager utan begränsningar som "
"rät linjeverktyget. Det använder inställningar av ritplattor på ett optimalt "
"sätt för att kontrollera penselns utseende. Använd "
"penselförinställningspanelen för att byta pensel."

#: ../../reference_manual/tools/freehand_brush.rst:27
msgid "Hotkeys and Sticky keys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/freehand_brush.rst:29
msgid "The freehand brush tool's hotkey is :kbd:`B`."
msgstr "Frihandspenselverktygets snabbtangent är :kbd:`B`."

#: ../../reference_manual/tools/freehand_brush.rst:31
msgid ""
"The alternate invocation is the ''color picker'' (standardly invoked by the :"
"kbd:`Ctrl` key). Press the :kbd:`Ctrl` key to switch the tool to \"color "
"picker\", use left or right click to pick fore and background color "
"respectively. Release the :kbd:`Ctrl` key to return to the freehand brush "
"tool."
msgstr ""
"Den alternativa sättet att anropa är \"färghämtaren\" (normalt anropad med  "
"tangenten :kbd:`Ctrl`). Tryck på tangenten :kbd:`Ctrl` för att byta till "
"verktyget \"färghämtare\", använd vänster- eller högerklick för att välja "
"respektive förgrunds- eller bakgrundsfärg. Släpp tangenten :kbd:`Ctrl` för "
"att återgå till frihandspenselverktyget."

#: ../../reference_manual/tools/freehand_brush.rst:32
msgid ""
"The Primary setting is \"size\" (standardly invoked by the :kbd:`Shift` "
"key). Press the :kbd:`Shift` key and drag outward to increase brush size. "
"Drag inward to decrease it."
msgstr ""
"Huvudinställningen är \"storlek\" (normalt anropad med tangenten :kbd:"
"`Shift`). Tryck på tangenten :kbd:`Shift` och dra utåt för att öka "
"penselstorleken. Dra inåt för att minska den."

#: ../../reference_manual/tools/freehand_brush.rst:33
msgid ""
"You can also press the :kbd:`V` key as a stickykey for the straight-line "
"tool."
msgstr ""
"Man kan också trycka på tangenten :kbd:`V` som är klistrig tangent för rät "
"linjeverktyget."

#: ../../reference_manual/tools/freehand_brush.rst:35
msgid ""
"The hotkey can be edited in :menuselection:`Settings --> Configure Krita --> "
"Configure Shortcuts`. The sticky-keys can be edited in :menuselection:"
"`Settings --> Configure Krita --> Canvas Input Settings`."
msgstr ""
"Snabbtangenterna kan redigeras i  :menuselection:`Inställningar --> Anpassa "
"Krita --> Anpassa genvägar`. Klistriga tangenter kan redigeras i  :"
"menuselection:`Inställningar --> Anpassa Krita --> Inställningar för "
"dukinmatning`."

#: ../../reference_manual/tools/freehand_brush.rst:39
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/freehand_brush.rst:41
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Stabilizer"
msgstr "Stabilisator"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "Basic Smooth"
msgstr "Grundutjämning"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "No Smoothing"
msgstr "Ingen utjämning"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "Weighted Smoothing"
msgstr "Viktad utjämning"

#: ../../reference_manual/tools/freehand_brush.rst:45
msgid "Smoothing"
msgstr "Utjämning"

#: ../../reference_manual/tools/freehand_brush.rst:47
msgid ""
"Smoothing, also known as stabilising in some programs, allows the program to "
"correct the stroke. Useful for people with shaky hands, or particularly "
"difficult long lines."
msgstr ""
"Utjämning, också känt som stabilisering i vissa program, gör det möjligt för "
"programmet att korrigera penseldraget. Det är användbart för personer med "
"skakiga händer, eller särskilt svåra långa linjer."

#: ../../reference_manual/tools/freehand_brush.rst:49
msgid "The following options can be selected:"
msgstr "Följande alternativ kan väljas:"

#: ../../reference_manual/tools/freehand_brush.rst:51
msgid "No Smoothing."
msgstr "Ingen utjämning."

#: ../../reference_manual/tools/freehand_brush.rst:52
msgid ""
"The input from the tablet translates directly to the screen. This is the "
"fastest option, and good for fine details."
msgstr ""
"Inmatning från ritplattan översätts direkt till skärmen. Det är det "
"snabbaste alternativet, och bra för fina detaljer."

#: ../../reference_manual/tools/freehand_brush.rst:53
msgid "Basic Smoothing."
msgstr "Grundutjämning."

#: ../../reference_manual/tools/freehand_brush.rst:54
msgid ""
"This option will smooth the input of older tablets like the Wacom Graphire "
"3. If you experience slightly jagged lines without any smoothing on, this "
"option will apply a very little bit of smoothing to get rid of those lines."
msgstr ""
"Det här alternativet jämnar ut inmatning från äldre ritplattor som Wacom "
"Graphire 3. Om man upplever något ojämna linjer utan något utjämning på, "
"lägger alternativet till en mycket liten utjämning för att bli av med dessa "
"linjer."

#: ../../reference_manual/tools/freehand_brush.rst:56
msgid ""
"This option allows you to use the following parameters to make the smoothing "
"stronger or weaker:"
msgstr ""
"Det här alternativet låter dig använda följande parametrar för att göra "
"utjämningen starkare eller svagare:"

#: ../../reference_manual/tools/freehand_brush.rst:58
#: ../../reference_manual/tools/freehand_brush.rst:70
msgid "Distance"
msgstr "Avstånd"

#: ../../reference_manual/tools/freehand_brush.rst:59
msgid ""
"The distance the brush needs to move before the first dab is drawn. "
"(Literally the amount of events received by the tablet before the first dab "
"is drawn.)"
msgstr ""
"Avståndet som penseln måste röra sig innan första klicken ritas "
"(bokstavligen antal händelser mottagna av ritplattan innan klicken ritas)."

#: ../../reference_manual/tools/freehand_brush.rst:60
msgid "Stroke Ending"
msgstr "Dragslut"

#: ../../reference_manual/tools/freehand_brush.rst:61
msgid ""
"This controls how much the line will attempt to reach the last known "
"position of the cursor after the left-mouse button/or stylus is lifted. Will "
"currently always result in a straight line, so use with caution."
msgstr ""
"Det här styr hur mycket linjen försöker nå den senast kända markörpositionen "
"efter vänster musknapp släpps eller pennan lyfts. Resulterar för närvarande "
"alltid i en rät linje, så använd med försiktighet."

#: ../../reference_manual/tools/freehand_brush.rst:62
msgid "Smooth Pressure"
msgstr "Jämt tryck"

#: ../../reference_manual/tools/freehand_brush.rst:63
msgid ""
"This will apply the smoothing on the pressure input as well, resulting in "
"more averaged size for example."
msgstr ""
"Låter utjämningen också gälla tryckinmatningen, vilket exempelvis resulterar "
"i mer genomsnittlig storlek."

#: ../../reference_manual/tools/freehand_brush.rst:65
msgid "Weighted smoothing:"
msgstr "Viktad utjämning:"

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Scalable Distance"
msgstr "Skalbart avstånd"

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid ""
"This makes it so that the numbers involved will be scaled along the zoom "
"level."
msgstr "Det här gör så att inblandade värden skalas enligt zoomnivå."

#: ../../reference_manual/tools/freehand_brush.rst:68
msgid ""
"This option averages all inputs from the tablet. It is different from "
"weighted smoothing in that it allows for always completing the line. It will "
"draw a circle around your cursor and the line will be a bit behind your "
"cursor while painting."
msgstr ""
"Det här alternativet medelvärdesbildar all inmatning från ritplattan. Det "
"skiljer sig från viktad utjämning eftersom det tillåter att linjen alltid "
"görs färdig. Det ritar en cirkel omkring markören och linjen är något efter "
"markören under målning."

#: ../../reference_manual/tools/freehand_brush.rst:71
msgid "This is the strength of the smoothing."
msgstr "Det här är utjämningens styrka."

#: ../../reference_manual/tools/freehand_brush.rst:72
msgid "Delay"
msgstr "Fördröjning"

#: ../../reference_manual/tools/freehand_brush.rst:73
msgid ""
"This toggles and determines the size of the dead zone around the cursor. "
"This can be used to create sharp corners with more control."
msgstr ""
"Det här kopplar om och bestämmer storleken på den döda zonen omkring "
"markören. Det kan användas för att skapa skarpa hörn med bättre kontroll."

#: ../../reference_manual/tools/freehand_brush.rst:74
msgid "Finish Line"
msgstr "Avsluta linje"

#: ../../reference_manual/tools/freehand_brush.rst:75
msgid "This ensures that the line will be finished."
msgstr "Det här säkerställer att linjen avslutas."

#: ../../reference_manual/tools/freehand_brush.rst:76
msgid "Stabilize sensors"
msgstr "Stabilisera sensorer"

#: ../../reference_manual/tools/freehand_brush.rst:77
msgid ""
"Similar to :guilabel:`Smooth Pressure`, this allows the input (pressure, "
"speed, tilt) to be smoother."
msgstr ""
"Liknar :guilabel:`Smooth Pressure`, och tillåter att inmatning (tryck, "
"hastighet, lutning) blir jämnare."

#: ../../reference_manual/tools/freehand_brush.rst:81
msgid "Painting Assistants"
msgstr "Målningsguider"

#: ../../reference_manual/tools/freehand_brush.rst:84
msgid "Assistants"
msgstr "Guider"

#: ../../reference_manual/tools/freehand_brush.rst:86
msgid ""
"Ticking this will allow snapping to :ref:`assistant_tool`, and the hotkey to "
"toggle it is :kbd:`Ctrl + Shift + L`. See :ref:`painting_with_assistants` "
"for more information."
msgstr ""
"Att markera det här tillåter låsning med :ref:`assistant_tool`, och "
"snabbtangenten för att koppla om det är :kbd:`Ctrl + Shift + L`. Ta en titt "
"på :ref:`painting_with_assistants` för mer information."

#: ../../reference_manual/tools/freehand_brush.rst:88
msgid ""
"The slider will determine the amount of snapping, with 1000 being perfect "
"snapping, and 0 being no snapping at all. For situations where there is more "
"than one assistant on the canvas, the defaultly ticked :guilabel:`Snap "
"Single` means that Krita will only snap to a single assistant at a time, "
"preventing noise. Unticking it allows you to chain assistants together and "
"snap along them."
msgstr ""
"Det här skjutreglaget bestämmer låsningens storlek, med 1000 är perfekt "
"låsning och 0 är ingen låsning alls. För situationer där det finns mer än en "
"guide på duken, gör det normalt aktiverade :guilabel:`Enstaka lås` att Krita "
"bara låser till en enstaka guide åt gången, vilket förhindrar brus. Att "
"avmarkera det tillåter att guider kedjas tillsammans, med låsning längs dem."
