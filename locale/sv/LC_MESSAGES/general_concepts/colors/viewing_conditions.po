# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-12 21:29+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_example_metamerism.png"
msgstr ".. image:: images/color_category/Krita_example_metamerism.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mixup_ex1_02.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_02.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_metamerism_presentation.svg"
msgstr ".. image:: images/color_category/Krita_metamerism_presentation.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:1
msgid "What are viewing conditions."
msgstr "Vad är visningsförhållanden?"

#: ../../general_concepts/colors/viewing_conditions.rst:10
#: ../../general_concepts/colors/viewing_conditions.rst:15
msgid "Viewing Conditions"
msgstr "Visningsförhållanden"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Metamerism"
msgstr "Metameri"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Color"
msgstr "Färg"

#: ../../general_concepts/colors/viewing_conditions.rst:17
msgid ""
"We mentioned viewing conditions before, but what does this have to do with "
"'white points'?"
msgstr ""
"Vi nämnde visningsförhållanden förut, men vad har det att göra med "
"'vitpunkter'?"

#: ../../general_concepts/colors/viewing_conditions.rst:19
msgid ""
"A lot actually, rather, white points describe a type of viewing condition."
msgstr ""
"En hel del i själva verket, vitpunkter beskriver faktiskt ett sorts "
"visningsförhållande."

#: ../../general_concepts/colors/viewing_conditions.rst:21
msgid ""
"So, usually what we mean by viewing conditions is the lighting and "
"decoration of the room that you are viewing the image in. Our eyes try to "
"make sense of both the colors that you are looking at actively (the colors "
"of the image) and the colors you aren't looking at actively (the colors of "
"the room), which means that both sets of colors affect how the image looks."
msgstr ""
"Så vad vi oftast menar med visningsförhållanden är ljussättningen och "
"inredningen i rummet där man tittar på bilden. Våra ögon försöker tolka både "
"färgerna som man betraktar aktivt (bildens färger) och färgerna som man inte "
"betraktar aktivt (rummets färger), vilket betyder att båda uppsättningarna "
"färger påverkar hur bilden ser ut."

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"
msgstr ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ""
"**Left**: Let's ruin Vermeer by putting a bright purple background that asks "
"for more attention than the famous painting itself. **Center**: a much more "
"neutral backdrop that an interior decorator would hate but brings out the "
"colors. **Right**: The approximate color that this painting is displayed "
"against in real life in the Maurits House, at the least, last time I was "
"there. Original image from wikipedia commons."
msgstr ""
"**Vänster**: Låt oss förstöra Vermeer genom att använda en ljuslila bakgrund "
"som pockar på mer uppmärksamhet än den berömda målningen själv. **Mitten**: "
"En mycket neutralare bakgrund som en inredningsarkitekt skulle hata, men som "
"framhäver färgerna. **Höger**: Ungefär den verkliga färgen bakom målningen i "
"Mauritshuis, åtminstone senast jag var där. Originalbild från Wikimedia "
"Commons."

#: ../../general_concepts/colors/viewing_conditions.rst:29
msgid ""
"This is for example, the reason why museum exhibitioners can get really "
"angry at the interior decorators when the walls of the museum are painted "
"bright red or blue, because this will drastically change the way how the "
"painting's colors look. (Which, if we are talking about a painter known for "
"their colors like Vermeer, could result in a really bad experience)."
msgstr ""
"Det är bland annat orsaken att museiintendenter skulle bli rätt irriterade "
"på inredningsarkitekterna om museets väggar målats ljusröda eller blåa, "
"eftersom det drastiskt skulle ändra hur målningarnas färger upplevs (vilket "
"skulle kunna ge en riktigt dålig upplevelse, om vi talar om en målare som "
"Vermeer, känd för sina färger)."

#: ../../general_concepts/colors/viewing_conditions.rst:37
msgid ""
"Lighting is the other component of the viewing condition which can have "
"dramatic effects. Lighting in particular affects the way how all colors "
"look. For example, if you were to paint an image of sunflowers and poppies, "
"print that out, and shine a bright yellow light on it, the sunflowers would "
"become indistinguishable from the white background, and the poppies would "
"look orange. This is called `metamerism <https://en.wikipedia.org/wiki/"
"Metamerism_%28color%29>`_, and it's generally something you want to avoid in "
"your color management pipeline."
msgstr ""
"Ljussättning är visningsförhållandenas andra komponent som kan få dramatiska "
"konsekvenser. Ljussättning påverkar i synnerhet hur alla färger ser ut. Om "
"man exempelvis målar en bild med solrosor och vallmo, skriver ut den, och "
"belyser den med ett starkt gult ljus, skulle solrosorna bli omöjliga att "
"särskilja från den vita bakgrunden, och vallmon skulle se orange ut. Det "
"kallas `metameri <https://sv.wikipedia.org/wiki/Metameri>`_, och är i "
"allmänhet någonting man vill undvika i sitt färghanteringsflöde."

#: ../../general_concepts/colors/viewing_conditions.rst:39
msgid ""
"An example where metamerism could become a problem is when you start "
"matching colors from different sources together."
msgstr ""
"Ett exempel där metameri skulle kunna bli ett problem är när man börjar "
"matcha ihop färger från olika källor."

#: ../../general_concepts/colors/viewing_conditions.rst:46
msgid ""
"For example, if you are designing a print for a red t-shirt that's not "
"bright red, but not super grayish red either. And you want to make sure the "
"colors of the print match the color of the t-shirt, so you make a dummy "
"background layer that is approximately that red, as correctly as you can "
"observe it, and paint on layers above that dummy layer. When you are done, "
"you hide this dummy layer and sent the image with a transparent background "
"to the press."
msgstr ""
"Antag exempelvis att man designar ett tryck för en röd t-tröja som inte är "
"ljusröd, men inte heller extremt gråaktigt röd, och man vill säkerställa att "
"tryckets färger matchar t-tröjans färg. Man skapar ett extra bakgrundslager "
"som ungefär har den röda färgen, så nära som man kan observera, och målar på "
"lager ovanför extralagret. När man är klar, döljer man extralagret och "
"skickar bilden med en genomskinlig bakgrund till tryckpressen."

#: ../../general_concepts/colors/viewing_conditions.rst:54
msgid ""
"But when you get the t-shirt from the printer, you notice that all your "
"colors look off, mismatched, and maybe too yellowish (and when did that T-"
"Shirt become purple?)."
msgstr ""
"Men när man får tillbaka t-tröjan från tryckeriet, märker man att alla "
"färger ser felmatchade ut, och kanske alltför gulaktiga (och när blev t-"
"tröjan lila?)."

#: ../../general_concepts/colors/viewing_conditions.rst:56
msgid "This is where white points come in."
msgstr "Det är här vitpunkter kommer in."

#: ../../general_concepts/colors/viewing_conditions.rst:58
msgid ""
"You probably observed the t-shirt in a white room where there were "
"incandescent lamps shining, because as a true artist, you started your work "
"in the middle of the night, as that is when the best art is made. However, "
"incandescent lamps have a black body temperature of roughly 2300-2800K, "
"which makes them give a yellowish light, officially called White Point A."
msgstr ""
"T-tröjan observerades troligen i ett vitt rum med lysande glödlampor, "
"eftersom en verklig konstnär alltid börjar arbeta mitt på natten, då den "
"bästa konsten alltid skapas då. Dock har glödlampor en svartkroppstemperatur "
"på ungefär 2300 - 2800 K, vilket gör att de avger ett gulaktigt ljus, "
"officiellt benämnt vitpunkt A."

#: ../../general_concepts/colors/viewing_conditions.rst:61
msgid ""
"Your computer screen on the other hand, has a black body temperature of "
"6500K, also known as D65. Which is a far more blueish color of light than "
"the lamps you are hanging."
msgstr ""
"Datorskärmar har å andra sidan en svartkroppstemperatur på 6500 K, också "
"känd som D65, vilket är en mycket blåaktigare färg än lamporna man sätter "
"upp."

#: ../../general_concepts/colors/viewing_conditions.rst:63
msgid ""
"What's worse, Printers print on the basis of using a white point of D50, the "
"color of white paper under direct sunlight."
msgstr ""
"Ännu värre, tryckerier baserar tryck på användning av vitpunkten D50, färgen "
"som vitt papper har i direkt solljus."

#: ../../general_concepts/colors/viewing_conditions.rst:70
msgid ""
"So, by eye-balling your t-shirt's color during the evening, you took its red "
"color as transformed by the yellowish light. Had you made your observation "
"in diffuse sunlight of an overcast (which is also roughly D65), or made it "
"in direct sunlight light and painted your picture with a profile set to D50, "
"the color would have been much closer, and thus your design would not be as "
"yellowish."
msgstr ""
"Så genom att använda ögonmått på t-tröjans färg på kvällen, fick man den "
"röda färgen som den omvandlades av det gulaktiga ljuset. Hade observationen "
"gjorts i diffust solljus en mulen dag (vilket också är ungefär D65), eller i "
"direkt solljus och målat bilden med profilen inställd till D50, hade färgen "
"varit mycket närmare, och sålunda hade designen inte blivit så gulaktig."

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ".. image:: images/color_category/White_point_mixup_ex1_03.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_03.png"

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ""
"Applying a white balance filter will sort of match the colors to the tone as "
"in the middle, but you would have had a much better design had you designed "
"against the actual color to begin with."
msgstr ""
"Att använda ett vitbalansfilter matchar ungefär färgerna till tonen som i "
"mitten, men man skulle få en mycket bättre design om man använde den "
"verkliga färgen från början."

#: ../../general_concepts/colors/viewing_conditions.rst:79
msgid ""
"Now, you could technically quickly fix this by using a white balancing "
"filter, like the ones in G'MIC, but because this error is caught at the end "
"of the production process, you basically limited your use of possible colors "
"when you were designing, which is a pity."
msgstr ""
"Tekniskt sett skulle man kunna fixa det genom att använda ett "
"vitbalansfilter, såsom de i G'MIC, men eftersom felet upptäcks i slutet av "
"produktionsprocessen, begränsar man i själva verket de färger som är möjliga "
"att använda när man designar, vilket är synd."

#: ../../general_concepts/colors/viewing_conditions.rst:81
msgid ""
"Another example where metamerism messes things up is with screen projections."
msgstr "Ett annat exempel då metameri ställer till det är skärmprojektion."

#: ../../general_concepts/colors/viewing_conditions.rst:83
msgid ""
"We have a presentation where we mark one type of item with red, another with "
"yellow and yet another with purple. On a computer the differences between "
"the colors are very obvious."
msgstr ""
"Vi har en presentation där vi markerar en sorts objekt med rött, ett annat "
"med gult och ett tredje med lila. På en dator är skillnaden mellan färgerna "
"mycket tydlig."

#: ../../general_concepts/colors/viewing_conditions.rst:89
msgid ""
"However, when we start projecting, the lights of the room aren't dimmed, "
"which means that the tone scale of the colors becomes crunched, and yellow "
"becomes near indistinguishable from white. Furthermore, because the light in "
"the room is slightly yellowish, the purple is transformed into red, making "
"it indistinguishable from the red. Meaning that the graphic is difficult to "
"read."
msgstr ""
"När vi projicerar den är dock rummets ljus inte dämpat, vilket betyder att "
"färgernas tonskala blir hoptryckt, och gult blir nästan omöjligt att skilja "
"från vitt. Eftersom ljuset i rummet är något gulaktigt blir dessutom lila "
"omvandlat till rött, vilket gör det omöjligt att skilja från rött. Det "
"betyder att grafiken blir svårläst."

#: ../../general_concepts/colors/viewing_conditions.rst:91
msgid ""
"In both cases, you can use Krita's color management a little to help you, "
"but mostly, you just need to be ''aware'' of it, as Krita can hardly fix "
"that you are looking at colors at night, or the fact that the presentation "
"hall owner refuses to turn off the lights."
msgstr ""
"I båda fallen kan man använda Kritas färghantering lite grand för att hjälpa "
"till, men i huvudsak måste man bara vara \"medveten\" om det, eftersom Krita "
"inte kan fixa att man tittar på färger mitt i natten, eller det faktum att "
"ljuset inte stängs av i hörsalen under presentationen."

#: ../../general_concepts/colors/viewing_conditions.rst:93
msgid ""
"That said, unless you have a display profile that uses LUTs, such as an OCIO "
"LUT or a cLUT icc profile, white point won't matter much when choosing a "
"working space, due to weirdness in the icc v4 workflow which always converts "
"matrix profiles with relative colorimetric, meaning the white points are "
"matched up."
msgstr ""
"Som sagt, om man inte har en bildskärmsprofil som använder LUT:ar, såsom en "
"OCIO LUT eller en cLUT ICC-profil, spelar inte vitpunkten så stor roll när "
"en arbetsyta väljes, på grund av konstigheten i ICC v4 arbetsflödet, som "
"leder till att det alltid konverterar matrisprofiler relativt "
"kolorimetriskt, vilket betyder att vitpunkter alltid matchas."
